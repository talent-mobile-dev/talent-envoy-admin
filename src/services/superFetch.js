const customHeader = () => ({
  "Content-Type": "application/json",
  Accept: "application/json",
  Authorization: "Bearer " + localStorage.getItem("id_token") || undefined
});

const base = (method, url, data = {}) => {

  const header = {
    method,
    headers: customHeader()
  };
  const httpParam =
    method === "get"
      ? header
      : Object.assign({}, header, { body: JSON.stringify(data) });

  return fetch(`${url}`, httpParam)
    .then(response => response.json())
    .then(res => res)
    .catch(error => ({ error: error }));
};

const SuperFetch = {};
["get", "post", "put", "delete"].forEach(method => {
  SuperFetch[method] = base.bind(null, method);
});
export default SuperFetch;
