import Api from "../../settings";
import SuperFetch from "../superFetch";

class ChatHelper {
  userList = async () => {
    return await SuperFetch.get(`${Api.apiUrl}/user/list`).then(response => {
      return this.handleResponse(response);
    });
  };

  handleResponse = response => {
    let chatRooms = [];
    const addChatRoom = users => {
      for (let i = 0; i < users.length; i++) {
        const chatRoom = {
          otherUserInfo: {
            name: users[i].name,
            job_title:
              users[i].custom_attributes.job_title != null
                ? users[i].custom_attributes.job_title
                : "",
            profileImageUrl: users[i].avatar.image_url,
            phone: users[0].phone
          },
          lastMessage:
            users[i].companies.companies.length === 0
              ? "None"
              : users[i].companies.companies[0].name,
          lastMessageTime: users[i].created_at,
          id: users[i].id,
          user_id: users[i].user_id,
          companies: users[i].companies,
          location: users[i].location_data,
          email: users[i].email
        };
        if (!!chatRoom.otherUserInfo.name) {
          chatRooms.push(chatRoom);
        }
      }
    };

    const users = response.result1;
    addChatRoom(users);
    const nextUsersArray = response.result2;
    nextUsersArray.map(users => {
      addChatRoom(users);
    });

    return chatRooms;
  };

  conversationsList = async userId => {
    return await SuperFetch.post(`${Api.apiUrl}/message/list`, {
      userId
    }).then(response => {
      return this.convHandleResponse(response);
    });
  };

  convHandleResponse = response => {
    return response.conversations;
  };

  getMessage = async messageId => {
    return await SuperFetch.post(`${Api.apiUrl}/message`, {
      messageId
    }).then(response => {
      return this.messageHandleResponse(response);
    });
  };

  replyToMessage = async payload => {
    return await SuperFetch.post(
      `${Api.apiUrl}/message/reply-admin`,
      payload
    ).then(response => {
      return this.messageHandleResponse(response);
    });
  };

  messageHandleResponse = response => {
    const { conversation_parts, conversation_message } = response;
    let result = [];
    const init_message = {
      sender: conversation_message.author,
      text: conversation_message.body,
      messageTime: response.created_at,
      part_type: conversation_message.type
    };
    result.push(init_message);
    for (let i = 0; i < conversation_parts.total_count; i++) {
      const obj = {
        sender: conversation_parts.conversation_parts[i].author,
        text: conversation_parts.conversation_parts[i].body,
        messageTime: conversation_parts.conversation_parts[i].created_at,
        part_type: conversation_parts.conversation_parts[i].part_type
      };
      result.push(obj);
    }
    return result;
  };

  initMessage = async param => {
    return await SuperFetch.post(`${Api.apiUrl}/message/initiate`, param).then(
      response => {
        return response;
      }
    );
  };
}

export default new ChatHelper();
