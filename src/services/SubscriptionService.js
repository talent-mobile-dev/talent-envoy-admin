let statusListener = null;

export const subscribeToStatusListener = (client, session) => {
  if (!client || !session) {
    return console.log("error instances");
  }

  return statusListener;
};

export const unSubscribeToStatusListener = () => {
  if (statusListener) {
    statusListener.unsubscribe();
  }

  return statusListener;
};
