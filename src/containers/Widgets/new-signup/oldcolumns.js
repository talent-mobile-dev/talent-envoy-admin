import React from "react";
import Icon from "antd/lib/icon";

const iconStyle = {
  cursor: "pointer",
  padding: "10px",
  background: "#23B1D3",
  borderRadius: "50%",
  color: "white"
};

export const columns = [
  {
    title: "id",
    dataIndex: "id",
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => a.id.length - b.id.length
    // sortDirections: ['descend'],
    // sorter: (a, b) => a.id - b.id,
  },
  {
    title: "Name Lastname",
    dataIndex: "name",
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.name.indexOf(value) === 0,
    sorter: (a, b) => a.name.length - b.name.length
    // sortDirections: ['descend'],
  },
  {
    title: "title",
    dataIndex: "title",
    // defaultSortOrder: 'descend',
    onFilter: (value, record) => record.title.indexOf(value) === 0,
    sorter: (a, b) => a.title.length - b.title.length
    // sortDirections: ['descend'],
  },
  {
    title: "sign up date",
    dataIndex: "signupDate",

    // filterMultiple: false,
    onFilter: (value, record) => record.signupDate.indexOf(value) === 0,
    sorter: (a, b) => new Date(a.signupDate) - new Date(b.signupDate)
    // sortDirections: ['descend'],
  },
  {
    dataIndex: "link",
    // onFilter: (value, record) => record.signupDate.indexOf(value) === 0,
    // sorter: (a, b) => a.signupDate.length - b.signupDate.length,
    // sortDirections: ['descend', 'ascend']
    render() {
      return <Icon type="right" style={iconStyle} />;
    }
  }
];
