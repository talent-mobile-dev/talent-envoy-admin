import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
// import NewSignUpWidget from "./style";
import { columns } from "./columns";
import { CANDIDATES_QUERY } from "./Schema";
import TableVariation from "../../../components/Widgets/Table";

class NewSignup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(pagination, filters, sorter) {
    console.log("params", pagination, filters, sorter);
  }

  render() {
    const {
      data: { candidates, loading }
    } = this.props;
    const dataSource = Array.isArray(candidates)
      ? candidates.map(candidate => ({
          ...candidate,
          title: (candidate.position && candidate.position.title) || "",
          signupDate: candidate.createdAt,
          key: candidate.id
        }))
      : [];

    return (
      <div>
        <TableVariation
          columns={columns}
          loading={loading}
          dataSource={dataSource}
          onChange={this.onChange}
          isHeader={true}
        />
      </div>
    );
  }
}

export default compose(graphql(CANDIDATES_QUERY))(NewSignup);
