const options = [
  {
    key: "",
    label: "Home",
    leftIcon: "home"
  },
  {
    key: "candidates",
    label: "Candidates",
    leftIcon: "user"
  },
  // {
  //   key: "chat",
  //   label: "sidebar.chat",
  //   leftIcon: "message"
  // },
  {
    key: "chat",
    label: "Chat",
    leftIcon: "message"
  },
  // {
  //   key: "mailbox",
  //   label: "sidebar.email",
  //   leftIcon: "mail"
  // }
  {
    key: "recruiters",
    label: "Recruiters",
    leftIcon: "solution"
  }
];
export default options;
