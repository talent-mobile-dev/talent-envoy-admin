import styled from "styled-components";
import WithDirection from "../../settings/withDirection";

const SignInStyleWrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  background-color: #000a22 !important;

  .isoLoginContentWrapper {
    width: 500px;
    height: 100%;
    z-index: 10;
    position: relative;
  }

  .isoLogoWrapper {
    margin-top: 116px !important;
    width: 450px;
    display: flex;
    margin-bottom: 25px;
    margin-right: 25px;
    justify-content: center;
    flex-shrink: 0;
  }

  .isoTitleWrapper {
    margin-bottom: 25px;
    font-size: 32px;
    float: left;
    color: #252525;
  }

  .isoButtonWrapper {
    height: 50px;
    width: 350px;
    border-radius: 5px;
    background-color: #0139ba;
    color: #fff;
  }

  .isoLoginContent {
    min-height: 40%;
    width: 450px;
    border: 1px solid #eeeeee;
    border-radius: 5px;
    display: flex;
    flex-direction: column;
    padding: 50px 50px;
    position: relative;
    background-color: #ffffff;

    @media only screen and (max-width: 767px) {
      width: 100%;
      padding: 70px 20px;
    }

    .isoSignInForm {
      width: 100%;
      display: flex;
      flex-shrink: 0;
      flex-direction: column;

      .isoInputWrapper {
        margin-bottom: 15px;

        &:last-of-type {
          margin-bottom: 0;
        }

        input {
          height: 50px;
          width: 350px;
          &::-webkit-input-placeholder {
            margin-left: 15;
            color: #454545;
          }

          &:-moz-placeholder {
            margin-left: 15;
            color: #454545;
          }

          &::-moz-placeholder {
            margin-left: 15;
            color: #454545;
          }
          &:-ms-input-placeholder {
            margin-left: 15;
            color: #454545;
          }
        }
      }

      button {
        font-weight: 500;
      }
    }
  }
`;

export default WithDirection(SignInStyleWrapper);
