import React, { Component } from "react";
import "./global.scss";
import "./responsive.scss";
import { connect } from "react-redux";

import { Layout } from "antd";
import { Debounce } from "react-throttle";
import WindowResizeListener from "react-window-size-listener";

import _ from "lodash";

import { ThemeProvider } from "styled-components";

import { notification, Button } from "antd";

import authAction from "../../redux/auth/actions";
import appActions from "../../redux/app/actions";
import chatActions from "../../redux/chat/actions";

import Sidebar from "../Sidebar/Sidebar";
import AppRouter from "./AppRouter";
import { siteConfig } from "../../settings";
import themes from "../../settings/themes";
import { themeConfig } from "../../settings";
import AppHolder from "./commonStyle";

import client from "../../apolloClient";
import { MESSAGE_LISTENER } from "./Schema";

const { Content, Footer } = Layout;
const { logout } = authAction;
const { toggleAll } = appActions;
const {
  putNewMessage,
  removeNewMessage,
  addNewMessage,
  setSelectedChatroom,
  getMessage,
  setRedirect,
  chatInit
} = chatActions;

const key = `open${Date.now()}`;

export class App extends Component {
  state = {
    notified_time: 0,
    socketObject: null
  };

  componentDidMount = () => {
    const setState = (time, obj) => {
      this.setState({
        notified_time: time,
        socketObject: obj
      });
    };

    client
      .subscribe({
        query: MESSAGE_LISTENER
      })
      .subscribe({
        next(data) {
          const socketObject = data.data.message.data;
          setState(socketObject.item.updated_at, socketObject);
          var isSafari = /^((?!chrome|android).)*safari/i.test(
            navigator.userAgent
          );
          if (!isSafari) {
            var audio = new Audio("/sounds/open-ended.mp3");
            audio.play();
          }
        },
        error(err) {
          console.error("err", err);
        }
      });

    const { chatInit, redirect, setRedirect } = this.props;
    if (!redirect) {
      chatInit();
      setRedirect(true);
    } else {
      setRedirect(false);
      this.setState({ message: false });
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.notified_time !== this.state.notified_time) {
      const { socketObject } = this.state;
      const { conversation_parts } = socketObject.item.conversation_parts;
      const { conversation_message } = socketObject.item;
      const { putNewMessage, addNewMessage, selectedConversation } = this.props;

      let newMessageObj = undefined;
      if (conversation_parts.length > 0) {
        newMessageObj = {
          room_id: socketObject.item.user.id,
          conv_id: socketObject.item.id,
          id: conversation_parts[0].id,
          image: conversation_parts[0].image,
          name: conversation_parts[0].author.name,
          time: conversation_parts[0].created_at,
          message: conversation_parts[0].body
        };
      } else {
        newMessageObj = {
          room_id: socketObject.item.user.id,
          conv_id: socketObject.item.id,
          id: conversation_message.id,
          image: conversation_message.image,
          name: socketObject.item.user.name,
          time: conversation_message.created_at,
          message: conversation_message.body
        };
      }

      if (newMessageObj.conv_id === selectedConversation) {
        const obj = {
          sender: conversation_parts[0].author,
          text: conversation_parts[0].body,
          messageTime: conversation_parts[0].created_at,
          part_type: conversation_parts[0].part_type
        };
        addNewMessage(obj);
      }

      putNewMessage(newMessageObj);

      const btn = (
        <Button
          type="primary"
          size="small"
          onClick={() => this.handleViewClick(newMessageObj)}
        >
          View
        </Button>
      );

      notification.info({
        message: `New Message from ${newMessageObj.name}`,
        btn,
        key
      });
    }
  };

  handleViewClick = newMessageObj => {
    const { setSelectedChatroom, chatRooms, setRedirect } = this.props;
    const chatRoom = _.find(chatRooms, { id: newMessageObj.room_id });
    if (!this.props.redirect) {
      setRedirect(true);
    }
    notification.close(key);

    setSelectedChatroom(chatRoom);
  };

  render() {
    const { url } = this.props.match;
    const { redirect } = this.props;
    const appHeight = window.innerHeight;


    return (
      <div className="dashboard">
        <ThemeProvider theme={themes[themeConfig.theme]}>
          <AppHolder>
            <Layout style={{ height: appHeight }}>
              <Debounce time="1000" handler="onResize">
                <WindowResizeListener
                  onResize={windowSize => console.log({ windowSize })}
                />
              </Debounce>
              {/* <Topbar url={url} /> */}
              <Layout style={{ flexDirection: "row", overflowX: "hidden" }}>
                <Sidebar url={url} />
                <Layout
                  className="isoContentMainLayout"
                  style={{
                    height: window.innerHeight
                  }}
                >
                  <Content
                    className="isomorphicContent"
                    style={{
                      padding: "0px 0px",
                      flexShrink: "0",
                      background: "#f1f3f6",
                      position: "relative",
                      height: "90%"
                    }}
                  >
                    <AppRouter url={url} />
                  </Content>
                  {/* <Footer
                    style={{
                      marginTop: 50,
                      background: "#ffffff",
                      textAlign: "center",
                      borderTop: "1px solid #ededed",
                      bottom: 0
                    }}
                  >
                    {siteConfig.footerText}
                  </Footer> */}
                </Layout>
              </Layout>
            </Layout>
          </AppHolder>
        </ThemeProvider>
      </div>
    );
  }
}

export default connect(
  state => ({
    auth: state.Auth,
    height: state.App.height,
    selectedConversation: state.Chat.selectedConversation,
    chatRooms: state.Chat.chatRooms,
    redirect: state.Chat.redirect
  }),
  {
    logout,
    toggleAll,
    putNewMessage,
    removeNewMessage,
    addNewMessage,
    setSelectedChatroom,
    getMessage,
    setRedirect,
    chatInit
  }
)(App);
