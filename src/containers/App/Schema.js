import gql from "graphql-tag";

export const MESSAGE_LISTENER = gql`
  subscription {
    message {
      data
    }
  }
`;
