import React, { Component } from "react";
import { Route } from "react-router-dom";
import asyncComponent from "../../helpers/AsyncFunc";

const routes = [
  {
    path: "",
    component: asyncComponent(() => import("../dashboard"))
  },
  {
    path: "candidates",
    component: asyncComponent(() => import("../Candidates"))
  },
  {
    path: "candidates/:candidate_id",
    component: asyncComponent(() => import("../Candidates/CandidateDetail"))
  },
  {
    path: "mailbox",
    component: asyncComponent(() => import("../Mail"))
  },
  {
    path: "authCheck",
    component: asyncComponent(() => import("../AuthCheck"))
  },
  {
    path: "chat/:room_id/:conv_id",
    component: asyncComponent(() => import("../Chat"))
  },
  {
    path: "chat/:room_id",
    component: asyncComponent(() => import("../Chat"))
  },
  {
    path: "chat",
    component: asyncComponent(() => import("../Chat"))
  }
  ,
  {
    path: "recruiters",
    component: asyncComponent(() => import("../Recruiters"))
  }
];

class AppRouter extends Component {
  render() {
    const { url, style } = this.props;
    return (
      <div style={style}>
        {routes.map(singleRoute => {
          const { path, exact, ...otherProps } = singleRoute;
          return (
            <Route
              exact={exact === false ? false : true}
              key={singleRoute.path}
              path={`${url}/${singleRoute.path}`}
              {...otherProps}
            />
          );
        })}
      </div>
    );
  }
}

export default AppRouter;
