import React, { Component } from "react";

class AvailableTimes extends Component {
  render() {
    const { availability } = this.props;

    return (
      <div className="questions_answer mt-30">
        <div className="que_head clearfix">
          <h2 className="pull-left">Available Times</h2>{" "}
        </div>
        <div className="question_main">
          <div className="resume-page">
            <div className="available_times">
              <ul className="avail_uline">
                {availability && availability.Monday.isActive && (
                  <li>
                    <div className="clearfix">
                      <strong className="pull-left">Monday</strong>
                      <p className="pull-right">
                        {availability.Monday.intervals &&
                          availability.Monday.intervals.map((interval, key) => {
                            return (
                              <div key={key}>
                                {interval.start} - {interval.end} <br />
                              </div>
                            );
                          })}
                      </p>
                    </div>
                  </li>
                )}
                {availability && availability.Tuesday.isActive && (
                  <li>
                    <div className="clearfix">
                      <strong className="pull-left">Tuesday</strong>
                      <p className="pull-right">
                        {availability.Monday.intervals &&
                          availability.Monday.intervals.map((interval, key) => {
                            return (
                              <div key={key}>
                                {interval.start} - {interval.end} <br />
                              </div>
                            );
                          })}
                      </p>
                    </div>
                  </li>
                )}
                {availability && availability.Wednesday.isActive && (
                  <li>
                    <div className="clearfix">
                      <strong className="pull-left">Wednesday</strong>
                      <p className="pull-right">
                        {availability.Monday.intervals &&
                          availability.Monday.intervals.map((interval, key) => {
                            return (
                              <div key={key}>
                                {interval.start} - {interval.end} <br />
                              </div>
                            );
                          })}
                      </p>
                    </div>
                  </li>
                )}
                {availability && availability.Thursday.isActive && (
                  <li>
                    <div className="clearfix">
                      <strong className="pull-left">Thursday</strong>
                      <p className="pull-right">
                        {availability.Monday.intervals &&
                          availability.Monday.intervals.map((interval, key) => {
                            return (
                              <div key={key}>
                                {interval.start} - {interval.end} <br />
                              </div>
                            );
                          })}
                      </p>
                    </div>
                  </li>
                )}
                {availability && availability.Friday.isActive && (
                  <li>
                    <div className="clearfix">
                      <strong className="pull-left">Friday</strong>
                      <p className="pull-right">
                        {availability.Monday.intervals &&
                          availability.Monday.intervals.map((interval, key) => {
                            return (
                              <div key={key}>
                                {interval.start} - {interval.end} <br />
                              </div>
                            );
                          })}
                      </p>
                    </div>
                  </li>
                )}
                {availability && availability.Saturday.isActive && (
                  <li>
                    <div className="clearfix">
                      <strong className="pull-left">Saturday</strong>
                      <p className="pull-right">
                        {availability.Saturday.intervals &&
                          availability.Saturday.intervals.map(
                            (interval, key) => {
                              return (
                                <div key={key}>
                                  {interval.start} - {interval.end} <br />
                                </div>
                              );
                            }
                          )}
                      </p>
                    </div>
                  </li>
                )}
                {availability && availability.Sunday.isActive && (
                  <li>
                    <div className="clearfix">
                      <strong className="pull-left">Sunday</strong>
                      <p className="pull-right">
                        {availability.Sunday.intervals &&
                          availability.Sunday.intervals.map((interval, key) => {
                            return (
                              <div key={key}>
                                {interval.start} - {interval.end} <br />
                              </div>
                            );
                          })}
                      </p>
                    </div>
                  </li>
                )}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default AvailableTimes;
