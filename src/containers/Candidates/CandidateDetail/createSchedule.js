import React, { Component } from "react";
import { Modal, Button, Icon, TimePicker } from "antd";
import DayPicker from "react-day-picker";
import DayPickerInput from "react-day-picker/DayPickerInput";
import { formatDate, parseDate } from "react-day-picker/moment";
import moment from "moment";
import "react-day-picker/lib/style.css";

import {
  ModalBody,
  ModalFooterButton,
  ScheduleCards,
  ScheduleCard,
  ScheduleCardDate,
  ScheduleCardEmptyStatus,
  ScheduleCardTime,
  TimeRadioButtons,
  RecMailInput,
  CreateScheduleModal,
  Break,
  JobDetails
} from "./createScheduleStyle";

class CreateSchedule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCreateScheduleModal: false,
      selectedScheduleId: "",
      selectedScheduleDate: "",
      selectedScheduleStartTime: "",
      selectedScheduleEndTime: "",
      dayPickerVisible: "none",
      timePicker1Visible: "none",
      schedules: []
    };
  }

  WEEKDAYS_LONG = {
    en: [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ]
  };
  WEEKDAYS_SHORT = {
    en: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"]
  };
  MONTHS = {
    en: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ]
  };

  componentDidMount = () => {
    document.body.addEventListener("click", e => {
      let timePicker1 = document.getElementById("timePicker22");

      if (timePicker1 && timePicker1.contains(e.target)) {
      } else {
        this.setState({
          dayPickerVisible: "none",
          timePicker1Visible: "none",
          timePicker2Visible: "none"
        });
      }
    });
  };

  formatDay = (d, locale = "en") => {
    return `${this.WEEKDAYS_LONG[locale][d.getDay()]}, ${d.getDate()} ${
      this.MONTHS[locale][d.getMonth()]
    } ${d.getFullYear()}`;
  };

  handleOptionChange = event => {
    let { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleClick = e => {
    let selectedScheduleId = e.target.id;
    this.setState({
      selectedScheduleId: selectedScheduleId,
      showCreateScheduleModal: true
    });
  };

  handleDayClick = day => {
    const FORMAT = "YYYY ddd MMM DD HH:mm";
    this.setState({
      selectedScheduleDate: moment(day, FORMAT).format("DD MMMM YYYY")
    });
  };

  handleIcon1Click = () => {
    this.setState({
      timePicker1Visible: "block"
    });
    let timePicker1 = document.getElementById("timePicker11");
    while (timePicker1.firstChild)
      timePicker1.removeChild(timePicker1.firstChild);

    for (var i = 0; i < 48; i++) {
      let opt = document.createElement("option");
      opt.value = moment("00:00", "HH:mm")
        .add(i * 30, "m")
        .format("HH:mm");
      opt.innerHTML =
        moment("00:00", "HH:mm")
          .add(i * 30, "m")
          .format("HH:mm") +
        "-" +
        moment("00:00", "HH:mm")
          .add((i + 1) * 30, "m")
          .format("HH:mm");
      timePicker1.appendChild(opt);
    }
  };

  scheduleCards = () => {
    const { schedulesByCandidateId } = this.props.getschedulesByIdQuery;
    if (this.state.schedules !== schedulesByCandidateId)
      this.setState({ schedules: schedulesByCandidateId });

    if (!this.state.schedules) return;
    else
      return (
        <ScheduleCards>
          {this.state.schedules.map(card => {
            const FORMAT = "YYYY ddd MMM DD HH:mm";
            const scheduleDate = moment(card.date, FORMAT).format(
              "DD MMMM YYYY"
            );
            const scheduleStartTime = moment(card.date, FORMAT).format("HH:mm");
            const scheduleEndTime = moment(card.date, FORMAT)
              .add(30, "m")
              .format("HH:mm");

            return (
              <ScheduleCard>
                <div>
                  <ScheduleCardDate>{scheduleDate}</ScheduleCardDate>
                  <ScheduleCardEmptyStatus
                    style={{
                      backgroundColor: card.isAssigned ? "#FFDADA" : "#CCF5DD",
                      color: card.isAssigned ? "#F44F4F" : "#00D157"
                    }}
                  >
                    {card.isAssigned ? "Full" : "Empty"}
                  </ScheduleCardEmptyStatus>
                </div>
                <div className="scheduleCardTime">
                  {scheduleStartTime} - {scheduleEndTime}
                </div>
                <div
                  key={card.id}
                  id={card.id}
                  onClick={e => {
                    this.setState({
                      selectedScheduleDate: scheduleDate,
                      selectedScheduleStartTime: scheduleStartTime,
                      selectedScheduleEndTime: scheduleEndTime
                    });
                    this.handleClick(e);
                  }}
                  style={{
                    position: "absolute",
                    left: 0,
                    top: 0,
                    minHeight: "100%",
                    minWidth: "100%"
                  }}
                />
              </ScheduleCard>
            );
          })}
        </ScheduleCards>
      );
  };

  createScheduleModal = e => {
    return (
      <CreateScheduleModal>
        <Modal
          className="schedule-modal"
          style={{ width: "100%", minWidth: "700px" }}
          centered
          visible={this.state.showCreateScheduleModal}
          onCancel={() => this.setState({ showCreateScheduleModal: false })}
          footer={
            <div style={{ display: "flex", flexDirection: "row-reverse" }}>
              <ModalFooterButton
                onClick={() =>
                  this.setState({ showCreateScheduleModal: false })
                }
              >
                Schedule
              </ModalFooterButton>
              <ModalFooterButton
                onClick={() =>
                  this.setState({ showCreateScheduleModal: false })
                }
              >
                Cancel
              </ModalFooterButton>
            </div>
          }
        >
          <ModalBody>
            <div style={{ marginLeft: 20 }}>
              <div>
                <label
                  style={{
                    height: 23,
                    color: "#252525",
                    fontSize: 22,
                    fontWeight: 500,
                    margin: "20px 0"
                  }}
                >
                  Create Schedule
                </label>
              </div>
              <RecMailInput>
                <input type="email" placeholder="Recruiter Mail Adress" />
              </RecMailInput>
              <div>
                <p className="title-date">
                  {this.state.selectedScheduleDate}
                  <Icon
                    type="edit"
                    className="icon3"
                    onClick={() =>
                      this.setState({
                        dayPickerVisible: "block"
                      })
                    }
                  />
                </p>
              </div>
              <TimeRadioButtons>
                <span className="timeSlot1">
                  <p>
                    {this.state.selectedScheduleStartTime} {"-"}
                    {this.state.selectedScheduleEndTime}
                  </p>
                  <Icon
                    type="edit"
                    className="icon1"
                    onClick={this.handleIcon1Click}
                  />
                </span>
              </TimeRadioButtons>
            </div>

            <Break />
            <div
              id="date-time-picker"
              className="date-time-picker"
              style={{
                display: this.state.dayPickerVisible
              }}
            >
              <DayPicker
                locale="en"
                format="LL"
                selectedDays={this.state.selectedScheduleDate}
                onDayClick={this.handleDayClick}
              />
            </div>
            <div
              id="timePicker1"
              className="time-picker1"
              style={{
                display: this.state.timePicker1Visible
              }}
            >
              <select
                id="timePicker11"
                size="10"
                onChange={e =>
                  this.setState({
                    selectedScheduleStartTime: e.target.value,
                    selectedScheduleEndTime: moment(e.target.value, "HH:mm")
                      .add(30, "m")
                      .format("HH:mm")
                  })
                }
              />
            </div>

            <JobDetails>
              <div>
                <div>
                  <label> Job Details </label>
                  <input type="text" placeholder="Title" />
                  <input type="text" placeholder="Company" />
                  <input type="text" placeholder="Location" />
                  <input type="text" placeholder="Employment Type" />
                </div>
              </div>

              <div>
                <textarea placeholder="Job Detail" />
              </div>
            </JobDetails>
          </ModalBody>
        </Modal>
      </CreateScheduleModal>
    );
  };
  render() {

    return (
      <div className="questions_answer mt-30">
        <div className="que_head clearfix">
          <h2 className="pull-left">Create Schedule</h2>
        </div>
        {/* {this.scheduleCards()} */}
        {this.createScheduleModal()}
      </div>
    );
  }
}
export default CreateSchedule;
