import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
import { columns } from "./callsColumns";
import {
  CANDIDATES_QUERY,
  GET_CANDIDATES_CALLS_QUERY,
  UPDATE_CANDIDATE,
  UPDATE_CANDIDATE_BY_ID
} from "./Schema";
import Table from "../../../components/Widgets/Table";
import AssignNumberModal from "../../../components/Modal";
import { Button, Form, Input, notification } from "antd";
import "./call.scss";

class AssignNumberInnerForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      candidateVoxNumber:
        (props.candidate && props.candidate.candidateVoxNumber) || ""
    };
  }

  onChangeInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = (resetFields, modalShow) => {
    const { updateCandidateByIdMutation, candidate } = this.props;
    const { candidateVoxNumber } = this.state;

    if (candidateVoxNumber && candidate) {
      updateCandidateByIdMutation({
        variables: {
          candidateId: candidate.id,
          candidate: { candidateVoxNumber }
        }
      })
        .then(data => {
          resetFields();
          modalShow(false);

          notification.success({
            message: `Candidate Updated`
          });

        })
        .catch(error => {
          notification.error({
            message: `Update Candidate Error`,
            description: error.message
          });
          console.log(error);
        });
    }
  };

  render() {
    const {
      getFieldDecorator,
      validateFields,
      resetFields,
      setFieldsValue
    } = this.props.form;

    const { modalShow, candidate } = this.props;

    return (
      <Form className="assignNumber-form">
        <div style={{ marginBottom: 20 }}>
          Please enter candidate Talent Envoy phone number.
        </div>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("candidateVoxNumber", {
            rules: [
              {
                required: true,
                message: "Please enter valid phone number!"
              }
            ]
          })(
            <Input
              maxLength={13}
              minLength={10}
              type="phone"
              size="large"
              value={this.state.candidateVoxNumber}
              placeholder="Talent Envoy Phone Number ex. +16111111111"
              name="candidateVoxNumber"
              onChange={this.onChangeInput}
            />
          )}
          Current Number: {candidate && candidate.candidateVoxNumber}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          <Button
            className="addCandidateBt"
            type="primary"
            style={{ background: "#0139BA" }}
            block
            onClick={() => {
              validateFields((err, values) => {
                if (!err) {
                  this.handleSubmit(resetFields, modalShow);
                }
              });
            }}
          >
            Update Candidate
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
const WrappedAssignNumberInnerForm = Form.create({ name: "assign_number" })(
  compose(
    graphql(UPDATE_CANDIDATE_BY_ID, {
      name: "updateCandidateByIdMutation"
    })
  )(AssignNumberInnerForm)
);

class Calls extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
    this.onChange = this.onChange.bind(this);
    this.renderDataSource = this.renderDataSource.bind(this);
  }

  onChange(pagination, filters, sorter) {
    console.log("params", pagination, filters, sorter);
  }

  renderDataSource(data) {
    if (Array.isArray(data)) {
      return data.map(item => ({
        ...item
      }));
    }
    return [];
  }
  modalShow = value => {
    const { candidateRefetch } = this.props;

    this.setState({
      showAssignNumberModal: value
    });
  };
  render() {
    const { calls, loading, candidate } = this.props;

    this.props.render && this.props.render(this.renderDataSource(calls));

    return (
      <div>
        <div className="questions_answer mt-30">
          <div className="que_head clearfix">
            <h2 className="pull-left" style={{ marginLeft: 10 }}>
              Calls
            </h2>
            <a href="#" className="pull-right">
              <Button
                style={{ background: "#0139BA" }}
                type="primary"
                size="medium"
                className="btn-addNewCandidate"
                onClick={() => this.modalShow(true)}
              >
                Assign Number
              </Button>
              <AssignNumberModal
                onOk={() => this.modalShow(false)}
                OnCancel={() => this.modalShow(false)}
                visible={this.state.showAssignNumberModal}
                title={"Enter Phone Number"}
              >
                <WrappedAssignNumberInnerForm
                  candidate={candidate}
                  modalShow={this.modalShow}
                />
              </AssignNumberModal>
            </a>
          </div>

          <div className="question_main">
            <Table
              columns={columns}
              loading={loading}
              dataSource={this.renderDataSource(calls)}
              onChange={this.onChange}
              isHeader={false}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Calls;
