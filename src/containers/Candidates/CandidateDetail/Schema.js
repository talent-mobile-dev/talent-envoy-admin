import gql from "graphql-tag";

export const GET_SCHEDULES_BY_ID = gql`
  query schedulesByCandidateId($candidateId: ID!) {
    schedulesByCandidateId(candidateId: $candidateId) {
      candidate {
        id
        name
      }
      date
      id
      timeZone
      isAssigned
      jobCard {
        title
        location
        companyName
        description
        contractType
        positionType
      }
      recruiter {
        email
        name
      }
    }
  }
`;

export const GET_CANDIDATE_DETAIL = gql`
  query candidate($id: ID!) {
    candidate(id: $id) {
      id
      name
      email
      createdAt
      fcmToken
      answers {
        value
        question {
          title
          name
        }
      }
      availability
      notAvailability
      fcmTokenUpdatedAt
      status
      isOnline
      candidateVoxNumber
      additionalQuestions {
        title
        name
        type
        category
      }
    }
  }
`;

export const GET_CANDIDATE_RESUME_UPLOADS = gql`
  query candidateResumeUploadsById($candidateId: ID!) {
    candidateResumeUploadsById(candidateId: $candidateId) {
      filename
      url
    }
  }
`;

export const GET_CANDIDATES_CALLS_QUERY = gql`
  query candidateCallsById($candidateId: ID!) {
    candidateCallsById(candidateId: $candidateId) {
      createdAt
      caller
      duration
      status
      recordUrl
      recruiter {
        name
      }
      jobCard {
        title
        companyName
      }
    }
  }
`;

export const UPDATE_STATUS = gql`
  mutation($status: String!, $candidateId: ID!) {
    updateCandidateStatus(status: $status, candidateId: $candidateId) {
      status
    }
  }
`;

export const UPDATE_ONLINE_STATUS = gql`
  mutation($isOnline: Boolean!, $candidateId: ID!) {
    updateOnlineStatus(isOnline: $isOnline, candidateId: $candidateId) {
      isOnline
    }
  }
`;

export const UPDATE_CANDIDATE_BY_ID = gql`
  mutation($candidate: JSON!, $candidateId: ID!) {
    updateCandidateById(candidate: $candidate, candidateId: $candidateId) {
      id
      name
      email
      status
      isOnline
    }
  }
`;

export const STATUS_LISTENER_FOR_ADMIN = gql`
  subscription($candidateId: ID) {
    statusListenerForAdmin(candidateId: $candidateId) {
      isOnline
      status
    }
  }
`;

export const ADD_SCHEDULE = gql`
  mutation($candidateId: ID!, $date: String!, $timeZone: JSON) {
    addSchedule(candidateId: $candidateId, date: $date, timeZone: $timeZone) {
      id
    }
  }
`;

export const DELETE_SCHEDULE = gql`
  mutation($scheduleId: ID!) {
    deleteSchedule(scheduleId: $scheduleId) {
      message
      result
    }
  }
`;

export const UPDATE_SCHEDULE = gql`
  mutation(
    $scheduleId: ID!
    $recruiterEmail: String!
    $recruiterName: String!
    $recruiterNumber: String!
    $companyName: String!
    $location: String!
    $title: String!
    $contractType: JSON!
    $positionType: String!
    $jobDescription: String!
    $isAssigned: Boolean!
  ) {
    updateScheduleAdmin(
      scheduleId: $scheduleId
      recruiterEmail: $recruiterEmail
      recruiterName: $recruiterName
      recruiterNumber: $recruiterNumber
      companyName: $companyName
      location: $location
      title: $title
      contractType: $contractType
      positionType: $positionType
      jobDescription: $jobDescription
      isAssigned: $isAssigned
    ) {
      id
    }
  }
`;
