import styled from "styled-components";

export const TimeRadioButtons = styled.div`
  display: inline;
  background: none;
  position: relative;

  p {
    margin: 0 ;
    display: inline-block;
    min-width: 100px;
  }
  .option-input {
    -webkit-appearance: none;
    -moz-appearance: none;
    -ms-appearance: none;
    -o-appearance: none;
    appearance: none;
    position: relative;
    top: 8px;
    right: 0;
    bottom: 0;
    left: 4px;
    transition: all 0.15s ease-out 0s;
    color: #fff;
    cursor: pointer;
    display: inline-block;
    outline: none;
    position: relative;
    height: 24px;
    width: 24px;
    border: 2px solid #23b1d3;
    background-color: #ffffff;
    &:focus {
      outline: none;
    }
  }
  .option-input:hover {
    border-color: #00d157;
  }
  .option-input:checked {
    border-color: #00d157;
    &:focus {
      outline: none;
    }
  }
  .option-input:checked::before {
    height: 20px;
    width: 20px;
    position: absolute;
    content: "✔";
    display: inline-block;
    font-size: 20px;
    text-align: center;
    line-height: 20px;
    color: #00d157;
    top: 2px;
    left: 0px;
    &:focus {
      outline: none;
    }
  }
  .option-input:checked::after {
    background: #40e0d0;
    content: "";
    display: block;
    position: relative;
    z-index: 100;
    &:focus {
      outline: none;
    }
  }
  .option-input.radio {
    border-radius: 50%;
    margin: 0;
    &:focus {
      outline: none;
    }
  }
  .option-input.radio::after {
    border-radius: 50%;
    &:focus {
      outline: none;
    }
  }

  .icon1 {
    position:absolute;
    top:0;
    left:90px;
    color:blue;
    cursor:pointer;
    visibility: hidden;
  }

   .icon2 {
    position:absolute;
    top:0;
    left:283px;
    color:blue;
    cursor:pointer;
    visibility: hidden;
  }

  .timeSlot1:hover .icon1{    
      visibility: visible;
  }

  .timeSlot2:hover .icon2{    
    visibility: visible;
}

  .icon1 {
      position: absolute;
      left: 120;
      top: 0,

      &:hover {
        color:blue
      }
    }}
  }

`;

export const RecMailInput = styled.div`
  float: right;
  height: 50px;
  width: 300px;
  border: 1px solid #eeeeee;
  border-radius: 5px;
  background-color: #f9f9f9;
  padding: 16px 20px;
  input {
    border: none;
    height: 18px;
    width: 100%;
    color: #454545;
    font-size: 14px;
    font-weight: 300;
    line-height: 18px;
    &:focus {
      outline: none;
    }
  }
`;

export const CreateScheduleModal = styled.div`
  width: 100%;
`;

export const JobDetails = styled.div`
  width: 100%;
  display: flex;

  label {
    height: 23px;
    color: #252525;
    font-size: 18px;
    font-weight: 300;
    line-height: 23px;
  }

  & > div {
    margin: 0 0 0 20px;
    height: 300px;
  }
  input {
    display: block;
    width: 310px;
    padding-left: 10px;
    color: #454545;
  }

  textarea {
    min-width: 300px;
    min-height: 260px;
    margin: 28px 0;
    float: right;
  }
`;

export const Break = styled.div`
  height: 1px;
  width: 100%;
  background-color: #eeeeee;
  margin: 30px 0;
`;

export const ModalBody = styled.div`
  width: 100%;

  .icon3 {
    color: blue;
    cursor: pointer;
    visibility: hidden;
  }

  .title-date {
    height: 23px;
    color: #252525;
    font-size: 18px;
    font-weight: 300;
    margin: 0;
  }

  .title-date:hover .icon3 {
    visibility: visible;
  }

  .date-time-picker {
    position: absolute;
    left: 30px;
    top: 113px;
    background: white;
    border: 1px solid #454545;
    -webkit-box-shadow: 13px 13px 21px -11px rgba(0, 0, 0, 0.75);
    -moz-box-shadow: 13px 13px 21px -11px rgba(0, 0, 0, 0.75);
    box-shadow: 13px 13px 21px -11px rgba(0, 0, 0, 0.75);
  }

  .time-picker1 {
    position: absolute;
    left: 35px;
    top: 130px;
    padding: 0;
    background: white;
    border: 1px solid #454545;
    -webkit-box-shadow: 13px 13px 21px -11px rgba(0, 0, 0, 0.75);
    -moz-box-shadow: 13px 13px 21px -11px rgba(0, 0, 0, 0.75);
    box-shadow: 13px 13px 21px -11px rgba(0, 0, 0, 0.75);
  }
`;

export const ModalFooterButton = styled.button`
  height: 50px;
  width: 109px;
  border-radius: 5px;
  background-color: #0139ba;
  color: #ffffff;
  font-size: 16px;
  font-weight: 500;
  line-height: 20px;
  text-align: center;
  display: inline-block;
  margin: 0 10px;
`;

export const ScheduleCards = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-around;
  align-content: space-around;
  flex-wrap: wrap;
`;

export const ScheduleCard = styled.div`
  height: 85px;
  min-width: 326px;
  border: 1px solid #eeeeee;
  border-radius: 5px;
  background-color: #ffffff;
  margin: 15px;
  padding: 20px;
  position: relative;
`;
export const ScheduleCardDate = styled.div`
  color: #454545;
  font-size: 16px;
  font-weight: bold;
  display: inline-block;
`;
export const ScheduleCardEmptyStatus = styled.div`
  height: 24px;
  width: 61px;
  border-radius: 5px;
  display: inline-block;
  float: right;

  font-size: 14px;
  font-weight: 300;
  text-align: center;
  padding-top: 2px;
`;
export const ScheduleCardTime = styled.div`
  color: #454545;
  font-size: 16px;
`;
