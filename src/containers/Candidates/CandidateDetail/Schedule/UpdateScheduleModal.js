import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  Modal,
  Form,
  Input,
  Button,
  DatePicker,
  notification,
  Select,
  Checkbox,
  Row,
  Col
} from "antd";
import { ADD_SCHEDULE, UPDATE_SCHEDULE } from "../Schema";
import { compose, graphql } from "react-apollo";
import moment from "moment";
import timeZones from "./timezones.json";
import TextArea from "antd/lib/input/TextArea";

const { RangePicker } = DatePicker;
const Option = Select.Option;

const UpdateScheduleModalInner = ({
  form: { getFieldDecorator, validateFields, resetFields, setFieldsValue },
  visible,
  updateScheduleMutation,
  onCancel,
  selectedCardData
}) => {
  const [state, setState] = useState({});

  useEffect(() => {
    if (selectedCardData) {
      const FORMAT = "YYYY ddd MMM DD HH:mm";
      try {
        setFieldsValue({ date: moment(selectedCardData.date, FORMAT) });
        setFieldsValue({ timeZone: selectedCardData.timeZone.offset });
        if (selectedCardData.isAssigned) {
          setFieldsValue({
            recruiterEmail: selectedCardData.recruiter.email || "ed@gmail.com"
          });
          setFieldsValue({
            recruiterName: selectedCardData.recruiter.name || "Ed Yılmaz"
          });

          if (selectedCardData.jobCard) {
            setFieldsValue({
              title: selectedCardData.jobCard.title || "Software Developer"
            });
            setFieldsValue({
              companyName: selectedCardData.jobCard.companyName || "Google"
            });
            setFieldsValue({
              location: selectedCardData.jobCard.location || "New York"
            });
            setFieldsValue({
              contractType: selectedCardData.jobCard.contractType || "Full Time"
            });
            setFieldsValue({
              positionType: selectedCardData.jobCard.positionType || "12 Months"
            });
          }
        }
      } catch (error) {
        console.log(error);
      }
    }
  }, [selectedCardData]);

  return (
    <Modal
      title={"Create Schedule"}
      centered
      visible={visible}
      footer={null}
      onCancel={onCancel}
    >
      <Form className="assignNumber-form" layout="inline">
        <Form.Item>
          {getFieldDecorator("date", {
            rules: [
              {
                required: true,
                message: "Please enter valid Date!"
              }
            ]
          })(
            <DatePicker
              showTime
              placeholder="Select Time"
              onChange={value => {
                setFieldsValue({ date: value });
                return setState({ date: value });
              }}
              onOk={value => {
                setFieldsValue({ date: value });
                return setState({ date: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("timeZone", {
            rules: [
              {
                required: true,
                message: "Please enter valid TimeZone!"
              }
            ]
          })(
            <Select
              className="timeZone"
              name="timeZone"
              style={{ width: 180, backgroundColor: "white" }}
              placeholder="Select a Timezone"
              onSelect={value => {
                setFieldsValue({ timeZone: value.offset });
                return setState({ timeZone: value.offset });
              }}
            >
              {timeZones.map((timeZone, index) => (
                <Option value={timeZone.offset} key={index}>
                  {timeZone.text}
                </Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <br />
        <br />
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("recruiterEmail", {
            rules: [
              {
                required: true,
                message: "Please enter valid Email number!"
              }
            ]
          })(
            <Input
              type="email"
              size="default"
              placeholder="Recruiter Email"
              name="recruiterEmail"
              onChange={value => {
                setFieldsValue({ recruiterEmail: value });
                return setState({ recruiterEmail: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("recruiterName", {
            rules: [
              {
                required: true,
                message: "Please enter valid Name!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Recruiter Name"
              name="recruiterName"
              onChange={value => {
                setFieldsValue({ recruiterName: value });
                return setState({ recruiterName: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("recruiterNumber", {
            rules: [
              {
                required: true,
                message: "Please enter valid Phone!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Recruiter Number"
              name="recruiterNumber"
              onChange={value => {
                setFieldsValue({ recruiterNumber: value });
                return setState({ recruiterNumber: value });
              }}
            />
          )}
        </Form.Item>
        <hr />
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("title", {
            rules: [
              {
                required: true,
                message: "Please enter valid title!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Title"
              name="title"
              onChange={value => {
                setFieldsValue({ title: value });
                return setState({ title: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("companyName", {
            rules: [
              {
                required: true,
                message: "Please enter valid company name!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Company Name"
              name="companyName"
              onChange={value => {
                setFieldsValue({ companyName: value });
                return setState({ companyName: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("location", {
            rules: [
              {
                required: true,
                message: "Please enter valid location!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Location"
              name="location"
              onChange={value => {
                setFieldsValue({ location: value });
                return setState({ location: value });
              }}
            />
          )}
        </Form.Item>

        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("positionType", {
            rules: [
              {
                required: true,
                message: "Please enter valid Position Type!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Position Type"
              name="positionType"
              onChange={value => {
                setFieldsValue({ positionType: value });
                return setState({ positionType: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("contractFullTime")(
            <Input
              type="text"
              size="default"
              placeholder="Full Time Expectation"
              name="contractFullTime"
              onChange={value => {
                setFieldsValue({ contractFullTime: value });
                return setState({ contractFullTime: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("contractW2")(
            <Input
              type="text"
              size="default"
              placeholder="W2 Expectation"
              name="contractW2"
              onChange={value => {
                setFieldsValue({ contractW2: value });
                return setState({ contractW2: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("contractCorp")(
            <Input
              type="text"
              size="default"
              placeholder="Corp-Corp Expectation"
              name="contractCorp"
              onChange={value => {
                setFieldsValue({ contractCorp: value });
                return setState({ contractCorp: value });
              }}
            />
          )}
        </Form.Item>

        <Form.Item style={{ marginBottom: 10, width: 600 }} layout="horizontal">
          {getFieldDecorator("jobDescription", {
            rules: [
              {
                required: true,
                message: "Please enter valid Job Description!"
              }
            ]
          })(
            <TextArea
              style={{ width: 400, height: 300 }}
              type="text"
              size="default"
              placeholder="Job Description"
              name="jobDescription"
              onChange={value => {
                setFieldsValue({ jobDescription: value });
                return setState({ jobDescription: value });
              }}
            />
          )}
        </Form.Item>

        <hr />
        <Form.Item style={{ marginBottom: 10 }}>
          <Button
            type="primary"
            style={{ background: "#0139BA" }}
            block
            onClick={() => {
              validateFields((err, values) => {
                if (!err) {
                  console.log(values);
                  let contractType = {
                    contractFullTime: values.contractFullTime,
                    contractW2: values.contractW2,
                    contractCorp: values.contractCorp
                  };

                  updateScheduleMutation({
                    variables: {
                      scheduleId: selectedCardData.id,
                      recruiterEmail: values.recruiterEmail,
                      recruiterName: values.recruiterName,
                      recruiterNumber: values.recruiterNumber,
                      companyName: values.companyName,
                      location: values.location,
                      title: values.title,
                      contractType: contractType,
                      positionType: values.positionType,
                      jobDescription: values.jobDescription,
                      isAssigned: true
                    }
                  })
                    .then(data => {
                      notification.success({
                        placement: "bottomLeft",
                        message: `Info`,
                        description: `Time slot successfully scheduled.`
                      });
                    })
                    .catch(error => {
                      console.log(error);
                    });
                }
              });
            }}
          >
            Update Schedule (Assign JobCard)
          </Button>
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          <Button
            type="primary"
            style={{ background: "#0139BA" }}
            block
            onClick={onCancel}
          >
            Cancel
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

const UpdateScheduleModal = Form.create({ name: "update_schedule" })(
  compose(
    graphql(UPDATE_SCHEDULE, {
      name: "updateScheduleMutation"
    })
  )(UpdateScheduleModalInner)
);

export default UpdateScheduleModal;
