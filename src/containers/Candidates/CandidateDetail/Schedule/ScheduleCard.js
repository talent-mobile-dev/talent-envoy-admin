import React from "react";
import { Card } from "antd";

import styled from "styled-components";

const BaseCard = styled.div`
  height: 85px;
  min-width: 326px;
  border: 1px solid #eeeeee;
  border-radius: 5px;
  background-color: #ffffff;
  padding: 20px;
  position: relative;
`;

const ScheduleCardDate = styled.div`
  color: #454545;
  font-size: 16px;
  font-weight: bold;
  display: inline-block;
`;

const ScheduleCardStatus = styled.div`
  height: 24px;
  width: 61px;
  display: inline-block;
  float: right;
  font-size: 14px;
  font-weight: 300;
  text-align: center;
  padding-top: 2px;
`;

const ScheduleCardTime = styled.div`
  color: #454545;
  font-size: 16px;
`;

const GridStyle = {
  borderRadius: 5,
  margin: 24,
  padding: 0,
  width: "25%",
  cursor: "pointer"
};

const ScheduleCard = ({ cardData, onClick }) => {
  return (
    <Card.Grid style={GridStyle} onClick={onClick}>
      <BaseCard>
        <ScheduleCardDate>{cardData.formattedDate}</ScheduleCardDate>
        <ScheduleCardStatus
          style={{
            backgroundColor: cardData.isAssigned ? "#FFDADA" : "#CCF5DD",
            color: cardData.isAssigned ? "#F44F4F" : "#00D157"
          }}
        >
          {cardData.isAssigned ? "Full" : "Empty"}
        </ScheduleCardStatus>
        <div className="scheduleCardTime">
          {cardData.scheduleStartTime} - {cardData.scheduleEndTime}
        </div>
      </BaseCard>
    </Card.Grid>
  );
};

export default ScheduleCard;
