import React, { useState } from "react"
import { Card, Col, Row, Button } from "antd"
import ScheduleCard from "./ScheduleCard"
import { graphql, compose } from "react-apollo"
import moment from "moment"
import CreateScheduleModal from "./CreateScheduleModal"
import UpdateScheduleModal from "./UpdateScheduleModal"

var formatDate = (element, index, array) => {
  const FORMAT = "YYYY ddd MMM DD HH:mm"
  if (element.date) {
    element.formattedDate = moment(element.date, FORMAT).format("DD MMMM YYYY")
    element.scheduleStartTime = moment(element.date, FORMAT).format("HH:mm")
    element.scheduleEndTime = moment(element.date, FORMAT)
      .add(30, "m")
      .format("HH:mm")
  }

  return element
}

const ScheduleGridList = ({ listData, onClick, candidateId, refetch }) => {
  const [state, setState] = useState({
    createScheduleModalVisible: false,
    updateScheduleModalVisible: false,
    selectedCardData: null,
  })

  return (
    <React.Fragment>
      <CreateScheduleModal
        refetch={refetch}
        visible={state.createScheduleModalVisible}
        candidateId={candidateId}
        onCancel={() => setState({ createScheduleModalVisible: false })}
      />
      <UpdateScheduleModal
        visible={state.updateScheduleModalVisible}
        selectedCardData={state.selectedCardData}
        onCancel={() => setState({ updateScheduleModalVisible: false })}
      />

      <Card
        title="Candidate Schedules"
        extra={
          <Button
            type="primary"
            style={{ background: "#0139BA" }}
            block
            onClick={() => setState({ createScheduleModalVisible: true })}
          >
            Create Schedule (Time Slot)
          </Button>
        }
      >
        {listData &&
          listData.map((item, index) => {
            return (
              <ScheduleCard
                cardData={item}
                key={index}
                onClick={() =>
                  setState({
                    updateScheduleModalVisible: true,
                    selectedCardData: item,
                  })
                }
              />
            )
          })}
      </Card>
    </React.Fragment>
  )
}

export default ScheduleGridList
