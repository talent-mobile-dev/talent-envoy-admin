import React, { useState } from "react";
import { Card, Col, Row, Button } from "antd";
import ScheduleCard from "./ScheduleCard";
import { graphql, compose } from "react-apollo";
import moment from "moment";
import CreateScheduleModal from "./CreateScheduleModal";
import UpdateScheduleModal from "./UpdateScheduleModal";

var formatDate = (element, index, array) => {
  const FORMAT = "YYYY ddd MMM DD HH:mm";
  if (element.date) {
    element.formattedDate = moment(element.date, FORMAT).format("DD MMMM YYYY");
    element.scheduleStartTime = moment(element.date, FORMAT).format("HH:mm");
    element.scheduleEndTime = moment(element.date, FORMAT)
      .add(30, "m")
      .format("HH:mm");
  }

  return element;
};

const ScheduleGridList = ({ listData, onClick, candidateId, refetch }) => {
  const [state, setState] = useState({
    createScheduleModalVisible: false,
    updateScheduleModalVisible: false,
    selectedCardData: null
  });
  const listDataFormatted = listData.map(formatDate);

  return (
    <React.Fragment>
      <CreateScheduleModal
        refetch={refetch}
        visible={state.createScheduleModalVisible}
        candidateId={candidateId}
        onCancel={() => setState({ createScheduleModalVisible: false })}
      />
      <UpdateScheduleModal
        refetch={refetch}
        visible={state.updateScheduleModalVisible}
        selectedCardData={state.selectedCardData}
        onCancel={() => setState({ updateScheduleModalVisible: false })}
      />

      <Card
        title="Schedules"
        headStyle={{
          height: 23,
          color: "#252525",
          fontSize: 18,
          fontWeight: 500,
          height: 60
        }}
        extra={
          <Button
            style={{
              float: "right",
              border: "none",
              boxShadow: "none",
              borderRadius: "50%",
              height: 30,
              width: 30,
              backgroundColor: "#0139ba",
              color: "#ffffff",
              fontSize: "20px",
              fontWeight: "bold",
              textAlign: "center",
              padding: 0
            }}
            onClick={() =>
              setState({
                createScheduleModalVisible: true
              })
            }
          >
            +
          </Button>
        }
      >
        {listData &&
          listData.map((item, index) => {
            return (
              <ScheduleCard
                cardData={item}
                key={index}
                onClick={() =>
                  setState({
                    updateScheduleModalVisible: true,
                    selectedCardData: item
                  })
                }
              />
            );
          })}
      </Card>
    </React.Fragment>
  );
};

export default ScheduleGridList;
