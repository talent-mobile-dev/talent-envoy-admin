import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  Modal,
  Form,
  Input,
  Button,
  DatePicker,
  notification,
  Select
} from "antd";
import { ADD_SCHEDULE, UPDATE_SCHEDULE, DELETE_SCHEDULE } from "../Schema";
import { compose, graphql } from "react-apollo";
import moment from "moment";
import timeZones from "./timezones.json";
import "./schedule.scss";

const { RangePicker } = DatePicker;
const Option = Select.Option;

const UpdateScheduleModalInner = ({
  form: { getFieldDecorator, validateFields, resetFields, setFieldsValue },
  visible,
  updateScheduleMutation,
  deleteScheduleMutation,
  onCancel,
  selectedCardData,
  refetch
}) => {
  const [state, setState] = useState({});

  useEffect(() => {
    if (selectedCardData) {
      const FORMAT = "YYYY ddd MMM DD HH:mm";
      setFieldsValue({ date: moment(selectedCardData.date, FORMAT) });
      setFieldsValue({ timeZone: selectedCardData.timeZone.offset });
      selectedCardData.recruiter &&
        setFieldsValue({ recruiterEmail: selectedCardData.recruiter.email });
      selectedCardData.recruiter &&
        setFieldsValue({ recruiterName: selectedCardData.recruiter.name });
      selectedCardData.jobCard &&
        setFieldsValue({ title: selectedCardData.jobCard.title });
      selectedCardData.jobCard &&
        setFieldsValue({ company: selectedCardData.jobCard.companyName });
      selectedCardData.jobCard &&
        setFieldsValue({ location: selectedCardData.jobCard.location });
      selectedCardData.jobCard &&
        setFieldsValue({ contractType: selectedCardData.jobCard.contractType });
      selectedCardData.jobCard &&
        setFieldsValue({ positionType: selectedCardData.jobCard.positionType });
      selectedCardData.jobCard &&
        setFieldsValue({
          jobDescription: selectedCardData.jobCard.jobDescription
        });
    } else resetFields();
  }, [selectedCardData]);

  return (
    <Modal
      className="schedule-modal"
      title={"Update Schedule"}
      centered
      visible={visible}
      footer={null}
      onCancel={onCancel}
    >
      <Form layout="inline">
        <h4>Time Slot</h4>

        <Form.Item>
          {getFieldDecorator("date", {
            rules: [
              {
                required: true,
                message: "Please enter valid Date!"
              }
            ]
          })(
            <DatePicker
              className="modal-date-picker"
              showTime
              format={"[ ]DD MMMM YYYY [   Time:] HH:mm  "}
              showTime={{ format: "HH:mm" }}
              placeholder="Select Time"
              renderExtraFooter={() => "Appointment Duration: 30 minutes"}
              onChange={value => {
                setFieldsValue({ date: value });
                return setState({ date: value });
              }}
              onOk={value => {
                setFieldsValue({ date: value });
                return setState({ date: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item className="time-zone-picker-form-item">
          {getFieldDecorator("timeZone", {
            rules: [
              {
                required: true,
                message: "Please select a timezone!"
              }
            ]
          })(
            <Select
              className="time-zone-select"
              style={{ width: 310, backgroundColor: "white" }}
              name="timeZone"
              placeholder="Select a Timezone"
              onSelect={value => {
                setFieldsValue({ timeZone: value.offset });
                return setState({ timeZone: value.offset });
              }}
            >
              {timeZones.map((timeZone, index) => (
                <Option value={timeZone.offset} key={index}>
                  {timeZone.text}
                </Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <hr style={{ marginTop: 5 }} />
        <h4>Recruiter</h4>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("recruiterEmail", {
            rules: [
              {
                required: true,
                message: "Please enter valid Email number!"
              }
            ]
          })(
            <Input
              type="email"
              size="default"
              placeholder="Recruiter Email"
              name="recruiterEmail"
              onChange={value => {
                setFieldsValue({ recruiterEmail: value });
                return setState({ recruiterEmail: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("recruiterName", {
            rules: [
              {
                required: true,
                message: "Please enter valid Name!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Recruiter Name"
              name="recruiterName"
              onChange={value => {
                setFieldsValue({ recruiterName: value });
                return setState({ recruiterName: value });
              }}
            />
          )}
        </Form.Item>

        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("recruiterNumber", {
            rules: [
              {
                required: true,
                message: "Please enter valid Name!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Recruiter Number"
              name="recruiterNumber"
              onChange={value => {
                setFieldsValue({ recruiterNumber: value });
                return setState({ recruiterNumber: value });
              }}
            />
          )}
        </Form.Item>

        <hr />
        <h4>Job Details</h4>

        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("title", {
            rules: [
              {
                required: true,
                message: "Please enter valid title!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Title"
              name="title"
              onChange={value => {
                setFieldsValue({ title: value });
                return setState({ title: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("company", {
            rules: [
              {
                required: true,
                message: "Please enter valid company!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Company"
              name="company"
              onChange={value => {
                setFieldsValue({ company: value });
                return setState({ company: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("location", {
            rules: [
              {
                required: true,
                message: "Please enter valid location!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Location"
              name="location"
              onChange={value => {
                setFieldsValue({ location: value });
                return setState({ location: value });
              }}
            />
          )}
        </Form.Item>

        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("positionType", {
            rules: [
              {
                required: true,
                message: "Please enter valid Position Type!"
              }
            ]
          })(
            <Input
              type="text"
              size="default"
              placeholder="Position Type"
              name="positionType"
              onChange={value => {
                setFieldsValue({ positionType: value });
                return setState({ positionType: value });
              }}
            />
          )}
        </Form.Item>

        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("contractFullTime")(
            <Input
              type="text"
              size="default"
              placeholder="Full Time Expectation"
              name="contractFullTime"
              onChange={value => {
                setFieldsValue({ contractFullTime: value });
                return setState({ contractFullTime: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("contractW2")(
            <Input
              type="text"
              size="default"
              placeholder="W2 Expectation"
              name="contractW2"
              onChange={value => {
                setFieldsValue({ contractW2: value });
                return setState({ contractW2: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item style={{ marginBottom: 10 }}>
          {getFieldDecorator("contractCorp")(
            <Input
              type="text"
              size="default"
              placeholder="Corp-Corp Expectation"
              name="contractCorp"
              onChange={value => {
                setFieldsValue({ contractCorp: value });
                return setState({ contractCorp: value });
              }}
            />
          )}
        </Form.Item>
        <Form.Item layout="horizontal">
          {getFieldDecorator("jobDescription", {
            rules: [
              {
                required: true,
                message: "Please enter valid Job Description!"
              }
            ]
          })(
            <Input.TextArea
              rows={4}
              type="text"
              style={{ width: 418 }}
              placeholder="Job Description"
              name="jobDescription"
              onChange={value => {
                setFieldsValue({ jobDescription: value });
                return setState({ jobDescription: value });
              }}
            />
          )}
        </Form.Item>

        <hr />
        <div style={{ minHeight: 40 }}>
          <Form.Item style={{ marginBottom: 10, float: "right" }}>
            <Button
              type="primary"
              style={{ background: "#0139BA" }}
              block
              onClick={() => {
                validateFields((err, values) => {
                  if (!err) {
                    let contractType = {
                      contractFullTime: values.contractFullTime,
                      contractW2: values.contractW2,
                      contractCorp: values.contractCorp
                    };
                    updateScheduleMutation({
                      variables: {
                        scheduleId: selectedCardData.id,
                        recruiterEmail: values.recruiterEmail,
                        recruiterName: values.recruiterName,
                        companyName: values.company,
                        location: values.location,
                        title: values.title,
                        contractType: values.contractType,
                        positionType: values.positionType,
                        jobDescription: values.jobDescription,
                        isAssigned: true
                      }
                      /* ,
                      update: (store) => {
                        // Read the data from our cache for this query.
                        const data = store.readQuery({
                          query: "getCandidateDetailQuery"
                        });
                       
                        store.writeQuery({ query: "getCandidateDetailQuery" });
                      } */
                    })
                      .then(data => {
                        notification.success({
                          placement: "bottomLeft",
                          message: `Info`,
                          description: `Time slot successfully scheduled.`
                        });
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  }
                });
                refetch();
                onCancel();
              }}
            >
              Update Schedule
            </Button>
          </Form.Item>

          <Form.Item style={{ marginBottom: 10, float: "right" }}>
            <Button
              type="primary"
              style={{ background: "#0139BA" }}
              block
              onClick={() => {
                deleteScheduleMutation({
                  variables: {
                    scheduleId: selectedCardData.id
                  }
                })
                  .then(data => {
                    notification.success({
                      placement: "bottomLeft",
                      message: `Info`,
                      description: `Schedule is successfully deleted `
                    });

                    console.log(data);
                  })
                  .catch(error => {
                    console.log(error);
                  });
                refetch();
                onCancel();
              }}
            >
              Delete Schedule
            </Button>
          </Form.Item>

          <Form.Item style={{ marginBottom: 10, float: "right" }}>
            <Button
              type="primary"
              style={{ background: "#0139BA" }}
              block
              onClick={onCancel}
            >
              Cancel
            </Button>
          </Form.Item>
        </div>
      </Form>
    </Modal>
  );
};

const UpdateScheduleModal = Form.create({ name: "update_schedule" })(
  compose(
    graphql(UPDATE_SCHEDULE, {
      name: "updateScheduleMutation"
    }),
    graphql(DELETE_SCHEDULE, {
      name: "deleteScheduleMutation"
    })
  )(UpdateScheduleModalInner)
);

export default UpdateScheduleModal;
