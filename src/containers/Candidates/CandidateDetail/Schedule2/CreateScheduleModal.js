import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  Modal,
  Form,
  Input,
  Button,
  DatePicker,
  Select,
  notification
} from "antd";
import { ADD_SCHEDULE } from "../Schema";
import { compose, graphql } from "react-apollo";
import timeZones from "./timezones.json";
import moment from "moment";
import "./schedule.scss";

const { RangePicker } = DatePicker;
const Option = Select.Option;
const FORMAT = "YYYY ddd MMM DD HH:mm";

const CreateScheduleModalInner = ({
  form: { getFieldDecorator, validateFields, resetFields, setFieldsValue },
  addScheduleMutation,
  candidateId,
  visible,
  onCancel,
  refetch
}) => {
  const [state, setState] = useState({ timeZone: -8 });
  const range = (start, end) => {
    const result = [];
    for (let i = start; i < end; i++) {
      result.push(i);
    }
    return result;
  };
  const disabledDateTime = () => {
    return {
      disabledMinutes: () => range(1, 30).concat(range(31, 60)),
      disabledSeconds: () => range(1, 60)
    };
  };

  return (
    <Modal
      className="create-schedule-modal"
      title={"Create Schedule"}
      centered
      visible={visible}
      footer={null}
      onCancel={onCancel}
    >
      <Form className="create-schedule-form" layout="inline">
        <Form.Item>
          {getFieldDecorator("date", {
            rules: [
              {
                required: true,
                message: "Please enter valid Date!"
              }
            ]
          })(
            <DatePicker
              className="modal-date-picker"
              format={"[ ]DD MMMM YYYY [   Time:] HH:mm  "}
              showTime={{ format: "HH:mm" }}
              renderExtraFooter={() => "Appointment Duration: 30 minutes"}
              placeholder="Select Time"
              onChange={value => {
                setFieldsValue({ date: value });
                return setState({ date: value });
              }}
              onOk={value => {
                setFieldsValue({ date: value });
                return setState({ date: value });
              }}
            />
          )}
        </Form.Item>

        <Form.Item
          className="time-zone-picker-form-item"
          style={{ marginBottom: 10 }}
        >
          {getFieldDecorator("timeZone", {
            rules: [
              {
                required: true,
                message: "Please enter valid TimeZone!"
              }
            ]
          })(
            <Select
              className="time-zone-select"
              name="timeZone"
              style={{ width: 310, backgroundColor: "white" }}
              placeholder="Select a Timezone"
              onSelect={value => {
                setFieldsValue({ timeZone: value.offset });
                return setState({ timeZone: value.offset });
              }}
            >
              {timeZones.map((timeZone, index) => (
                <Option value={timeZone.offset} key={index}>
                  {timeZone.text}
                </Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <hr />
        <div style={{ minHeight: 50 }}>
          <Form.Item style={{ marginBottom: 10, float: "right" }}>
            <Button
              className="addScheduleButton"
              type="primary"
              style={{ background: "#0139BA" }}
              block
              onClick={() => {
                validateFields((err, values) => {
                  const newTimeZone = timeZones.filter(
                    tz => tz.offset == values.timeZone
                  );

                  if (!err) {
                    addScheduleMutation({
                      variables: {
                        candidateId: candidateId,
                        date: values.date.format(FORMAT),
                        timeZone: newTimeZone[0]
                      }
                    })
                      .then(data => {
                        notification.success({
                          placement: "bottomLeft",
                          message: `Info`,
                          description: `Timeslot created.`
                        });
                        console.log("addScheduleMutation");
                        console.log(data);

                        refetch();
                        onCancel();
                      })
                      .catch(error => {
                        console.log(error);
                      });
                  }
                });
              }}
            >
              Create Schedule
            </Button>
          </Form.Item>
          <Form.Item style={{ marginBottom: 10, float: "right" }}>
            <Button
              type="primary"
              style={{ background: "#0139BA" }}
              block
              onClick={onCancel}
            >
              Cancel
            </Button>
          </Form.Item>
        </div>
      </Form>
    </Modal>
  );
};

const CreateScheduleModal = Form.create({ name: "create_schedule" })(
  compose(
    graphql(ADD_SCHEDULE, {
      name: "addScheduleMutation"
    })
  )(CreateScheduleModalInner)
);

export default CreateScheduleModal;
