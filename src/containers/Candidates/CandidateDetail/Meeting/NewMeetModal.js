import React, { useState } from "react"
import { Modal, Button } from "antd"
import PropTypes from "prop-types"
import styled from "@emotion/styled"
import NewMeetForm from "./NewMeetForm"

const ButtonStyled = styled(Button)`
  float: left;
`

const NewMeetModal = (props) => {
  const [state, setState] = useState({ loading: false, visible: false })

  const handleOk = () => {
    setState({ loading: true })
    setTimeout(() => {
      setState({ loading: false, visible: false })
    }, 3000)
  }

  const handleCancel = () => {
    setState({ visible: false })
  }

  const showModal = () => {
    setState({
      visible: true,
    })
  }
  return (
    <div>
      <ButtonStyled type="primary" onClick={showModal}>
        New Meeting
      </ButtonStyled>
      <Modal visible={state.visible} title="Create Meeting" onOk={handleOk} onCancel={handleCancel}>
        <NewMeetForm />
      </Modal>
    </div>
  )
}

NewMeetModal.propTypes = {}

export default NewMeetModal
