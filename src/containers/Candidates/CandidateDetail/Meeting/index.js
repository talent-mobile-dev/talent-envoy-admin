import React, { useState, useEffect } from "react"
import { useQuery } from "react-apollo-hooks"
import { Card, Modal, Button } from "antd"
import PropTypes from "prop-types"
import styled from "@emotion/styled"
import { GET_MEETINGS } from "./queries"
import NewMeetModal from "./NewMeetModal"

const CardGrid = styled(Card.Grid)`
  width: 25%;
  text-align: center;
`

const Meeting = (props) => {
  const { data, error, loading } = useQuery(GET_MEETINGS, {
    variables: { candidateId: props.candidateId },
  })
  if (loading) {
    return <div>Loading...</div>
  }

  if (error) {
    return <div>Error! {error.message}</div>
  }

  if (!data) {
    return <div>Data Not Found</div>
  }

  if (data.meetings) {
    return (
      <Card
        title={
          <div>
            <NewMeetModal />
          </div>
        }
      >
        {data.meetings.map((meet) => (
          <CardGrid>
            {meet.candidate.name} - {meet.recruiter.name}
          </CardGrid>
        ))}
      </Card>
    )
  }
}

Meeting.propTypes = {}

export default Meeting
