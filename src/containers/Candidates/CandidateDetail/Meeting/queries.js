import gql from "graphql-tag"

export const GET_MEETINGS = gql`
  query meetings($candidateId: ID!) {
    meetings(candidateId: $candidateId) {
      id
      recruiter {
        name
      }
      candidate {
        name
      }
      jobCard {
        title
        contractType
        description
      }
    }
  }
`

export const ADD_MEET = gql`
  mutation addMeet(
    $candidateEmail: String!
    $recruiterEmail: String!
    $date: String!
    $title: String!
    $companyName: String!
    $location: String!
    $positionType: String!
    $contractW2: String!
    $contractCorp: String!
    $description: String!
  ) {
    addMeet(
      candidateEmail: $candidateEmail
      recruiterEmail: $recruiterEmail
      date: $date
      title: $title
      companyName: $companyName
      location: $location
      positionType: $positionType
      contractW2: $contractW2
      contractCorp: $contractCorp
      description: $description
    ) {
      id
      recruiter {
        name
      }
      candidate {
        name
      }
      jobCard {
        title
        contractType
        description
      }
    }
  }
`
