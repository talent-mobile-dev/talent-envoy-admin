import React from "react"
import { Form, Icon, Input, Button, Checkbox, DatePicker } from "antd"
import TextArea from "antd/lib/input/TextArea"
import { useMutation } from "react-apollo-hooks"
import PropTypes from "prop-types"
import { ADD_MEET } from "./queries"

const MeetForm = ({ form }) => {
  const addMeet = useMutation(ADD_MEET)
  const { getFieldDecorator, setFieldsValue } = form

  const handleSubmit = (e) => {
    e.preventDefault()
    form.validateFields((err, values) => {
      console.log(values)
      if (!err) {
        addMeet({ variables: { ...values, date: values.date.format("YYYY MM DD HH:mm") } })
        console.log("Received values of form: ", values)
      }
    })
  }

  return (
    <Form onSubmit={handleSubmit} className="newmeet-form">
      <Form.Item>
        {getFieldDecorator("candidateEmail", {
          initialValue: "edward.tiong@talentenvoy.com",
          rules: [{ required: true, message: "Please enter Candidate Email!" }],
        })(<Input placeholder="Candidate Email" />)}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator("recruiterEmail", {
          initialValue: "ashu@erostechnologies.com",
          rules: [{ required: true, message: "Please enter Recruiter Email!" }],
        })(<Input placeholder="recruiterEmail" />)}
      </Form.Item>
      <Form.Item>
        {getFieldDecorator("date", {
          rules: [
            {
              required: true,
              message: "Please enter valid Date!",
            },
          ],
        })(<DatePicker showTime placeholder="Select Time" />)}
      </Form.Item>
      <Form.Item style={{ marginBottom: 10 }}>
        {getFieldDecorator("title", {
          rules: [
            {
              required: true,
              message: "Please enter valid title!",
            },
          ],
        })(<Input type="text" size="default" placeholder="Title" name="title" />)}
      </Form.Item>
      <Form.Item style={{ marginBottom: 10 }}>
        {getFieldDecorator("companyName", {
          rules: [
            {
              required: true,
              message: "Please enter valid company name!",
            },
          ],
        })(<Input type="text" size="default" placeholder="Company Name" name="companyName" />)}
      </Form.Item>
      <Form.Item style={{ marginBottom: 10 }}>
        {getFieldDecorator("location", {
          rules: [
            {
              required: true,
              message: "Please enter valid location!",
            },
          ],
        })(<Input type="text" size="default" placeholder="Location" name="location" />)}
      </Form.Item>

      <Form.Item style={{ marginBottom: 10 }}>
        {getFieldDecorator("positionType", {
          rules: [
            {
              required: true,
              message: "Please enter valid Position Type!",
            },
          ],
        })(<Input type="text" size="default" placeholder="Position Type" name="positionType" />)}
      </Form.Item>
      <Form.Item style={{ marginBottom: 10 }}>
        {getFieldDecorator("contractFullTime")(
          <Input
            type="text"
            size="default"
            placeholder="Full Time Expectation"
            name="contractFullTime"
          />
        )}
      </Form.Item>
      <Form.Item style={{ marginBottom: 10 }}>
        {getFieldDecorator("contractW2")(
          <Input type="text" size="default" placeholder="W2 Expectation" name="contractW2" />
        )}
      </Form.Item>
      <Form.Item style={{ marginBottom: 10 }}>
        {getFieldDecorator("contractCorp")(
          <Input
            type="text"
            size="default"
            placeholder="Corp-Corp Expectation"
            name="contractCorp"
          />
        )}
      </Form.Item>

      <Form.Item style={{ marginBottom: 10, width: 600 }} layout="horizontal">
        {getFieldDecorator("description", {
          rules: [
            {
              required: true,
              message: "Please enter valid Job Description!",
            },
          ],
        })(
          <TextArea
            style={{ width: 400, height: 300 }}
            type="text"
            size="default"
            placeholder="Job Description"
            name="description"
          />
        )}
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Create
        </Button>
      </Form.Item>
    </Form>
  )
}

const NewMeetForm = Form.create({ name: "new_meet" })(MeetForm)

NewMeetForm.propTypes = {}

export default NewMeetForm
