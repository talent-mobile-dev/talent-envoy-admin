import React, { Component } from "react"
import { Tabs, Tab, Nav } from "react-bootstrap"
import { graphql, compose, Subscription, withApollo } from "react-apollo"
import Icon from "antd/lib/icon"
import { withRouter } from "react-router-dom"

import { connect } from "react-redux"
import actions from "../../../redux/chat/actions"

import {
  GET_SCHEDULES_BY_ID,
  GET_CANDIDATE_DETAIL,
  GET_CANDIDATE_RESUME_UPLOADS,
  GET_CANDIDATES_CALLS_QUERY,
  UPDATE_STATUS,
  UPDATE_ONLINE_STATUS,
  STATUS_LISTENER_FOR_ADMIN,
} from "./Schema"
import ProfileInfo from "./ProfileInfo"
import QuestionAns from "./QuestionAnswer"
import Calls from "./calls"
import { getStatusNameFromId } from "../../../helpers/statusHelper"
import { Spin, Select, Switch, notification, Button } from "antd"
import "./candidateDetail.scss"
import Criteria from "./criteria"
import Resume from "./resume"
import AvailableTimes from "./availableTimes"
import CreateSchedule from "./createSchedule"
import Meeting from "./Meeting"

const Option = Select.Option

const statusOptions = [
  { statusId: "0", statusDesc: "Onboarding Welcome" },
  { statusId: "1", statusDesc: "Waiting First Review" },
  { statusId: "1_1", statusDesc: "Waiting Update Resume" },
  { statusId: "1_2", statusDesc: "Waiting Additional Questions" },
  { statusId: "1_3", statusDesc: "Unfortunately Not Continue" },
  { statusId: "2", statusDesc: "Waiting QA - Start" },
  { statusId: "2_1", statusDesc: "Waiting QA" },
  { statusId: "2_2", statusDesc: "Waiting Missing QA" },
  { statusId: "3", statusDesc: "Waiting Second Review" },
  { statusId: "3_1", statusDesc: "Set The Batch Time" },
  { statusId: "3_2", statusDesc: "Onboarded - Waiting Batch" },
  { statusId: "4", statusDesc: "Onboarded - On The Batch" },
]

class CandidateDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      candidateId: props.match.params.candidate_id,
      candidate: null,
      isOnlineLoading: false,
      statusId: "",
      createScheduleModalVisible: true,
      updateScheduleModalVisible: false,
    }
  }

  componentDidMount() {
    const { getCandidateDetailQuery, match, client } = this.props
    console.log(getCandidateDetailQuery)
    getCandidateDetailQuery.refetch({ id: match.params.candidate_id }).then((data) => {
      if (data.data.candidate) {
        this.setState({
          candidate: data.data.candidate,
          statusId: data.data.candidate.status,
          isOnline: data.data.candidate.isOnline,
        })
      }
    })
    console.log("client")
    console.log(client)
    let that = this
    client
      .subscribe({
        query: STATUS_LISTENER_FOR_ADMIN,
        variables: { candidateId: match.params.candidate_id },
      })
      .subscribe({
        next(data) {
          console.log(data.data.statusListenerForAdmin && data.data.statusListenerForAdmin.isOnline)
          if (data && data.data.statusListenerForAdmin) {
            that.setState({
              isOnline: data.data.statusListenerForAdmin.isOnline,
            })
          }
        },
        error(err) {
          console.error("err", err)
        },
      })
  }
  handleStatusSelectChange = (statusId) => {
    console.log(`selected ${statusId}`)
    const { updateStatusMutation, match } = this.props
    updateStatusMutation({
      variables: { candidateId: match.params.candidate_id, status: statusId },
    })
      .then((data) => {
        notification.success({
          placement: "bottomLeft",
          message: `Info`,
          description: `Candidate status changed to ` + statusId,
        })
        this.setState({
          statusId,
        })
        console.log(data)
      })
      .catch((error) => {
        console.log(error)
      })
  }

  handleIsOnlineSwitchChange = (isOnline) => {
    const { updateOnlineStatusMutation, match } = this.props
    this.setState({
      isOnlineLoading: true,
    })
    updateOnlineStatusMutation({
      variables: { candidateId: match.params.candidate_id, isOnline },
    })
      .then((data) => {
        if (isOnline) {
          notification.success({
            placement: "bottomLeft",
            message: `Info`,
            description: "Candidate is Online",
          })
        } else {
          notification.warning({
            placement: "bottomLeft",
            message: `Warning`,
            description: "Candidate is Offline",
          })
        }

        this.setState({
          isOnline,
          isOnlineLoading: false,
        })
        console.log(data)
      })
      .catch((error) => {
        notification.success({
          placement: "bottomLeft",
          message: `Error`,
          description: error.message,
        })
        this.setState({
          isOnline: !isOnline,
          isOnlineLoading: false,
        })
        console.log(error)
      })
  }

  render() {
    const {
      getCandidateDetailQuery,
      getCandidateResumeUploadsQuery,
      getCandidateCallsQuery,
      updateOnlineStatusMutation,
      getSchedulesByIdQuery,
    } = this.props
    console.log(getSchedulesByIdQuery)

    const { candidate } = this.state

    if (
      !candidate ||
      getCandidateDetailQuery.loading ||
      getCandidateResumeUploadsQuery.loading ||
      getSchedulesByIdQuery.loading
    ) {
      return <Spin />
    }

    const findChatRoom = (userId) => {
      let roomArray = this.props.Chat.chatRooms.filter((room) => room.user_id == userId)
      console.log("find chat room returns:")
      console.log(roomArray[0])
      return roomArray[0]
    }

    const ChatButton2 = withRouter(({ history, record }) => {
      console.log(record)
      console.log(history)
      return findChatRoom(this.props.getCandidateCallsQuery.variables.candidateId) ? (
        <Icon
          type="wechat"
          style={{
            cursor: "pointer",
            background: "#0139BA",
            borderRadius: "50%",
            color: "white",
            marginRight: 20,

            height: 25,
            width: 25,
            fontSize: 20,
          }}
          onClick={() => {
            history.push("../chat")
            this.props.setRedirect(true)
            this.props.setSelectedChatroom(
              findChatRoom(this.props.getCandidateCallsQuery.variables.candidateId)
            )
          }}
        />
      ) : (
        ""
      )
    })

    return (
      <div>
        <div className="col-lg-10 col-md-9 vs_right_panel">
          <div className="dash_head">
            <div className="name">
              {" "}
              <span className="name-span"> {candidate && candidate.name}</span>
              <span className="status-span">
                {candidate && getStatusNameFromId(this.state.statusId)}
              </span>
            </div>

            <div
              className="status-select"
              style={{
                display: "flex",
                verticalAlign: "middle",
                alignItems: "center",
              }}
            >
              <ChatButton2 />

              <Switch
                loading={this.state.isOnlineLoading}
                checked={this.state.isOnline}
                onChange={this.handleIsOnlineSwitchChange}
                style={{
                  background: this.state.isOnline ? "#00D157" : "#F44F4F",
                  marginRight: 15,
                }}
              />
              <Select
                defaultValue="Status"
                value={this.state.statusId}
                dropdownMenuStyle={{ width: 240 }}
                style={{
                  background: "#0139BA",
                  width: 260,
                  marginTop: 5,
                  marginRight: 25,
                }}
                onChange={this.handleStatusSelectChange}
              >
                {statusOptions.map(({ statusId, statusDesc }) => {
                  return (
                    <Option key={statusId} value={statusId}>
                      {statusDesc}
                    </Option>
                  )
                })}
              </Select>
            </div>
          </div>

          <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
            <Tab eventKey="profile" title="Profile" mountOnEnter={true}>
              <div className="tab-content">
                <div id="profile" className="tab-pane fade in active">
                  <div className="profile_desc">
                    <ProfileInfo />
                    <div className="row">
                      <div className="col-md-6 col-sm-6">
                        <Criteria answers={candidate.answers} />
                      </div>
                      <div className="col-md-6 col-sm-6">
                        <Resume
                          uploads={getCandidateResumeUploadsQuery.candidateResumeUploadsById}
                        />
                      </div>
                    </div>
                    <Calls
                      calls={getCandidateCallsQuery.candidateCallsById}
                      loading={getCandidateCallsQuery.loading}
                    />
                  </div>
                </div>
              </div>
            </Tab>
            <Tab eventKey="calls" title="Calls">
              <div className="tab-content">
                <div id="profile" className="tab-pane fade in active">
                  <div className="profile_desc">
                    <Calls
                      candidateRefetch={getCandidateDetailQuery.refetch}
                      candidate={candidate}
                      calls={getCandidateCallsQuery.candidateCallsById}
                      loading={getCandidateCallsQuery.loading}
                    />
                  </div>
                </div>
              </div>
            </Tab>
            <Tab eventKey="resume" title="Resume" mountOnEnter={true}>
              <div className="tab-content">
                <div id="profile" className="tab-pane fade in active">
                  <div className="profile_desc">
                    <div className="row">
                      <div className="col-md-6 col-sm-6">
                        <Resume
                          uploads={getCandidateResumeUploadsQuery.candidateResumeUploadsById}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Tab>
            {/* <Tab eventKey="QA" title="Q & A">
              <div className="tab-content">
                <h2>Q & A</h2>
              </div>
            </Tab> */}
            <Tab eventKey="criteria" title="Criteria">
              <div className="tab-content">
                <div id="profile" className="tab-pane fade in active">
                  <div className="profile_desc">
                    <div className="row">
                      <div className="col-md-6 col-sm-6">
                        {candidate && candidate.answers.length > 0 && (
                          <Criteria answers={candidate.answers} />
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Tab>
            <Tab eventKey="availableTimes" title="Available Times">
              <div className="tab-content">
                <div id="profile" className="tab-pane fade in active">
                  <div className="profile_desc">
                    <div className="row">
                      <div className="col-md-6 col-sm-6">
                        {candidate && candidate.availability && (
                          <AvailableTimes availability={candidate.availability} />
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Tab>

            <Tab eventKey="meetings" title="Meetings">
              <div className="tab-content">
                <div id="profile" className="tab-pane fade in active">
                  <div className="profile_desc">
                    <div className="row">
                      <div className="col-md-12 col-sm-12">
                        {candidate && <Meeting candidateId={candidate.id} />}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Tab>
            {/* <Tab eventKey="NTR" title="Notes to recruiters">
              <div className="tab-content">
                <h2>Notes to recruiters</h2>
              </div>
            </Tab>
            <Tab eventKey="availableTimes" title="Available Times">
              <div className="tab-content">
                <h2>Available Times</h2>
              </div>
            </Tab>
            <Tab eventKey="QFR" title="Questions From Recruiters">
              <div className="tab-content">
                <h2>Questions From Recruiters</h2>
              </div>
            </Tab>
            <Tab eventKey="opportunity" title="Opportunity">
              <div className="tab-content">
                <h2>Opportunity</h2>
              </div>
            </Tab>
            <Tab eventKey="mailbox" title="Mailbox">
              <div className="tab-content">
                <h2>Mailbox</h2>
              </div>
            </Tab>
            <Tab eventKey="setting" title="Setting">
              <div className="tab-content">
                <h2>Setting</h2>
              </div>
            </Tab> */}
          </Tabs>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return state
}

export default compose(
  withApollo,
  graphql(GET_SCHEDULES_BY_ID, {
    name: "getSchedulesByIdQuery",
    options: (props) => ({
      fetchPolicy: "network-only",
      variables: { candidateId: props.match.params.candidate_id },
    }),
  }),
  graphql(GET_CANDIDATE_DETAIL, {
    name: "getCandidateDetailQuery",
    options: (props) => ({
      fetchPolicy: "network-only",
      variables: { id: props.match.params.candidate_id },
    }),
  }),
  graphql(GET_CANDIDATE_RESUME_UPLOADS, {
    name: "getCandidateResumeUploadsQuery",
    options: (props) => ({
      fetchPolicy: "network-only",
      variables: { candidateId: props.match.params.candidate_id },
    }),
  }),
  graphql(GET_CANDIDATES_CALLS_QUERY, {
    name: "getCandidateCallsQuery",
    options: (props) => ({
      fetchPolicy: "network-only",
      variables: { candidateId: props.match.params.candidate_id },
    }),
  }),
  graphql(UPDATE_STATUS, {
    name: "updateStatusMutation",
  }),
  graphql(UPDATE_ONLINE_STATUS, {
    name: "updateOnlineStatusMutation",
  }),
  connect(
    mapStateToProps,
    actions
  )
)(CandidateDetail)
