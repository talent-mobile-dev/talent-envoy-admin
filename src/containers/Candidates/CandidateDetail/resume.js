import React, { Component } from "react";
import { Document, Page } from "react-pdf";
import { GET_CANDIDATE_DETAIL, GET_CANDIDATE_RESUME_UPLOADS } from "./Schema";
import { Button, Icon } from "antd";
import { Nav, ButtonGroup } from "react-bootstrap";
import {
  Buttons,
  ButtonsGroup
} from "../../../components/uielements/styles/button.style";
import ActionButton from "antd/lib/modal/ActionButton";
import { compose } from "recompose";
import { graphql } from "react-apollo";

class Resume extends Component {
  state = {
    file: null,
    numPages: 0,
    pageNumber: 1
  };
  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages });
  };
  goToPrevPage = () =>
    this.setState(state => ({ pageNumber: state.pageNumber - 1 }));
  goToNextPage = () =>
    this.setState(state => ({ pageNumber: state.pageNumber + 1 }));
  render() {
    const { uploads } = this.props;

    const { pageNumber, numPages } = this.state;
    return (
      <div>
        {uploads && uploads.length > 0 && (
          <div className="questions_answer mt-30">
            <div className="que_head clearfix">
              <h2 className="pull-left">Resume</h2>{" "}
              <a href="#" className="pull-right">
                <Button
                  type="primary"
                  icon="cloud-download"
                  style={{ marginLeft: 10 }}
                />
              </a>
            </div>
            <div className="question_main">
              <div className="resume-page">
                <Document
                  file={uploads[0].url}
                  onLoadSuccess={this.onDocumentLoadSuccess}
                  noData={<h4>No File Found.</h4>}
                >
                  <Page pageNumber={pageNumber} />
                </Document>

                <div className="que_head clearfix mt-20">
                  <div className="pull-left" style={{ marginTop: 10 }}>
                    <Button type="primary" className="pull-left">
                      Send Request
                    </Button>
                    <Button.Group style={{ marginLeft: 50 }}>
                      <Button
                        disabled={pageNumber < 2}
                        type="primary"
                        className="primary-button"
                        onClick={this.goToPrevPage}
                      >
                        <Icon type="left" />
                        Prev
                      </Button>
                      <Button
                        disabled={pageNumber === numPages}
                        type="primary"
                        className="primary-button"
                        onClick={this.goToNextPage}
                      >
                        Next
                        <Icon type="right" />
                      </Button>
                    </Button.Group>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
export default Resume;
