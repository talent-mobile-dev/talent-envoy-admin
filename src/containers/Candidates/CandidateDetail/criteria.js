import React, { Component } from "react";
import { GET_CANDIDATE_DETAIL } from "./Schema";

class Criteria extends Component {
  render() {
    const { answers } = this.props;
    if (answers && answers.length === 0) {
      return null;
    }
    return (
      <div>
        <div className="questions_answer mt-30">
          <div className="que_head clearfix">
            <h2 className="pull-left">Criteria</h2>
            <a href="#" className="pull-right">
              View All
            </a>
          </div>

          <div className="question_main">
            {answers &&
              answers.map((answer, index) => {
                if (answer.question.name == "preferredEmployment") {
                  return (
                    <div className="ques_grid" key={index}>
                      <h3>{answer.question.title}</h3>
                      {answer.value.full && (
                        <p>
                          $ {answer.value.fullExpectation} - Full Time Salary
                        </p>
                      )}
                      {answer.value.contractW2 && (
                        <p>
                          $ {answer.value.contractW2Expectation} - Contract - W2
                          Salary
                        </p>
                      )}
                      {answer.value.corp && (
                        <p>
                          $ {answer.value.corpExpectation} - Corp - Corp Salary
                        </p>
                      )}
                    </div>
                  );
                }
                if (
                  answer.question.name == "currentLocation" &&
                  answer.value.selectedProvince &&
                  answer.value.selectedCity
                ) {
                  return (
                    <div className="ques_grid" key={index}>
                      <h3>{answer.question.title}</h3>
                      <p>
                        {answer.value.selectedProvince.name} -
                        {answer.value.selectedCity}
                      </p>
                    </div>
                  );
                }
                if (
                  answer.question.name == "relocate" &&
                  answer.value.locations
                ) {
                  return (
                    <div className="ques_grid" key={index}>
                      <h3>{answer.question.title}</h3>
                      {
                        <p>
                          {answer.value.locations[0].city}-
                          {answer.value.locations[0].province}
                        </p>
                      }
                    </div>
                  );
                }
                if (answer.question.name == "jobTitle") {
                  return (
                    <div className="ques_grid" key={index}>
                      <h3>{answer.question.title}</h3>
                      <p>{answer.value}</p>
                    </div>
                  );
                }
                if (
                  answer.question.name == "targetTitles" &&
                  answer.value &&
                  answer.value.length > 0
                ) {
                  return (
                    <div className="ques_grid" key={index}>
                      <h3>{answer.question.title}</h3>
                      {answer.value.map(title => (
                        <p>{title.name}</p>
                      ))}
                    </div>
                  );
                }
                if (answer.question.name == "totalExperience") {
                  return (
                    <div className="ques_grid" key={index}>
                      <h3>{answer.question.title}</h3>
                      <p>{answer.value}</p>
                    </div>
                  );
                }
                if (answer.question.name == "currentlyEmployed") {
                  return (
                    <div className="ques_grid" key={index}>
                      <h3>{answer.question.title}</h3>
                      {answer.value.choise === "yes" ? (
                        <p>Yes - {answer.value.companyName}</p>
                      ) : (
                        <p>No</p>
                      )}
                    </div>
                  );
                }
                if (answer.question.name == "statusIntheUs") {
                  return (
                    <div className="ques_grid" key={index}>
                      <h3>{answer.question.title}</h3>
                      {answer.value.choice === "noNeed" && (
                        <p>No Sponsorship Needed</p>
                      )}
                      {answer.value.choice === "h1b" && <p>H1B Work Permit</p>}
                      {answer.value.choice === "opt" && <p>OPT (3 Years)</p>}
                      {answer.value.choice === "Other" && (
                        <p>answer.value.otherExplain</p>
                      )}
                    </div>
                  );
                }
                if (answer.question.name == "typesOfCompaniesYouDontPrefer") {
                  return (
                    <div className="ques_grid" key={index}>
                      <h3>{answer.question.title}</h3>
                      {answer.value.startup && <p>Startup</p>}
                      {answer.value.small && <p>Small</p>}
                      {answer.value.enterprise && <p>Enterprise</p>}
                      {answer.value.other && <p>answer.value.otherExplain</p>}
                    </div>
                  );
                }
                if (
                  answer.question.name == "mainSkillset" &&
                  answer.value &&
                  answer.value.length > 0
                ) {
                  return (
                    <div className="ques_grid" key={index}>
                      <h3>{answer.question.title}</h3>
                      {answer.value.map(skill => (
                        <p>
                          {skill.name} - {skill.level}/10
                        </p>
                      ))}
                    </div>
                  );
                }
              })}
          </div>
        </div>
      </div>
    );
  }
}

export default Criteria;
