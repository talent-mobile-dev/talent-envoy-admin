import React from "react";
import Icon from "antd/lib/icon";
import { withRouter } from "react-router-dom";
import ReactAudioPlayer from "react-audio-player";
const iconStyle = {
  cursor: "pointer",
  padding: "10px",
  background: "#23B1D3",
  borderRadius: "50%",
  color: "white"
};

const ArrowButton = withRouter(({ history, record }) => {

  return (
    <Icon
      type="right"
      style={iconStyle}
      onClick={() => history.push("candidates/" + record.id)}
    />
  );
});

export const columns = [
  {
    title: "Call",
    dataIndex: "createdAt",
    onFilter: (value, record) => record.createdAt.indexOf(value) === 0,
    sorter: (a, b) => new Date(a.createdAt) - new Date(b.createdAt),
    render: (text, record) => {
      if (record.status === "missed") {
        return (
          <div className="red_text call_date">
            <img src="/images/missed_call.png" />
            {record.createdAt}
          </div>
        );
      } else {
        return (
          <div className="call_date">
            <img src="/images/outgoing_call.png" />
            {record.createdAt}
          </div>
        );
      }
    }
    // sortDirections: ['descend'],
    // sorter: (a, b) => a.id - b.id,
  },
  {
    title: "Call Status",
    dataIndex: "status",
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.status.indexOf(value) === 0,
    sorter: (a, b) => a.status.length - b.status.length,
    render: (text, record) => {
      if (record.status === "missed") {
        return <div className="cl_btn missed_btn">Missed</div>;
      } else if (record.status === "success") {
        return <div className="cl_btn complete_btn">Completed</div>;
      } else {
        return <div className="cl_btn Unavailable_btn">Unavailable</div>;
      }
    }
    // sortDirections: ['descend'],
  },
  {
    title: "Recruiter Name",
    dataIndex: "recruiter.name",
    // defaultSortOrder: 'descend',
    onFilter: (value, record) => record.recruiter.name.indexOf(value) === 0,
    sorter: (a, b) => a.recruiter.name.length - b.recruiter.name.length
    // sortDirections: ['descend'],
  },
  {
    title: "Duration",
    dataIndex: "duration",

    // filterMultiple: false,
    onFilter: (value, record) => record.duration.indexOf(value) === 0,
    sorter: (a, b) => a.duration - b.duration
    // sortDirections: ['descend'],
  },
  {
    title: "Record",
    dataIndex: "recordUrl",
    // filterMultiple: false,
    onFilter: (value, record) => record.recordUrl.indexOf(value) === 0,
    sorter: (a, b) => a.recordUrl.length - b.recordUrl.length,
    render: (text, record) => {
      return <ReactAudioPlayer src={record.recordUrl} controls />;
    }
    // sortDirections: ['descend'],
  },
  {
    title: "Position",
    dataIndex: "jobCard.title",
    // filterMultiple: false,
    onFilter: (value, record) => record.jobCard.title.indexOf(value) === 0,
    sorter: (a, b) => a.jobCard.title.length - b.jobCard.title.length,
    render: (text, record) => {
      return (
        <div>
          {record.jobCard.companyName} - {record.jobCard.title}
        </div>
      );
    }
    // sortDirections: ['descend'],
  },
  {
    title: "",
    key: "link",
    dataIndex: "link",
    onFilter: (value, record) => record.status.indexOf(value) === 0,
    // sorter: (a, b) => a.signupDate.length - b.signupDate.length,
    // sortDirections: ['descend', 'ascend']
    render: (text, record) => {
      return (
        <ul className="list-inline calls_link">
          <li>
            <a href="#">
              <img src="/images/message.svg" />
            </a>
          </li>
          <li>
            <img src="/images/calling.svg" />
          </li>
        </ul>
      );
    }
  }
];
