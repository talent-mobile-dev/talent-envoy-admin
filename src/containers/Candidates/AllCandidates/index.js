import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
import { columns } from "../columns";
import { CANDIDATES_QUERY } from "./Schema";
import Table from "../../../components/Widgets/Table";

class AllCandidates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
    this.onChange = this.onChange.bind(this);
    this.renderDataSource = this.renderDataSource.bind(this);
  }

  onChange(pagination, filters, sorter) {
    console.log("params", pagination, filters, sorter);
  }

  renderDataSource(data) {
    if (Array.isArray(data)) {
      return data.map(item => ({
        ...item,
        title: (item.position && item.position.title) || "",
        signupDate: item.createdAt,
        key: item.id
      }));
    }
    return [];
  }

  render() {
    const {
      data: { candidates, loading }
    } = this.props;

    this.props.render && this.props.render(this.renderDataSource(candidates));

    return (
      <div>
        <Table
          columns={columns}
          loading={loading}
          dataSource={this.renderDataSource(candidates)}
          onChange={this.onChange}
          isHeader={false}
        />
      </div>
    );
  }
}

export default compose(graphql(CANDIDATES_QUERY))(AllCandidates);
