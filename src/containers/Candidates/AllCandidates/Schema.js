import gql from "graphql-tag";

export const CANDIDATES_QUERY = gql`
  query candidates {
    candidates {
      token
      id
      email
      talentEmail
      createdAt
      updatedAt
      candidateVoxNumber
      isOnline
      # provider {
      #     google
      #     default
      # }
      name
      profileUrl
      position {
        id
        title
      }
      # answers
      # status
      # accessToken
      # fcmToken
      # fcmTokenUpdatedAt
      # availability
      # notAvailability
      # callLimitationPerWeek
      # isOnline
      # additionalQuestions
    }
  }
`;
