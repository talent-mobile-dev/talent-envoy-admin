import React from "react";
import { Input, Button } from "antd";

const { TextArea } = Input;

export const SendNotificationsInner = () => (
  <React.Fragment>
    <Button className="addCandidatesBtn" type="primary" block>
      Add Candidate
    </Button>
    <Input size="large" placeholder="Title" />
    <TextArea rows={4} size="large" placeholder="Description" />
    <Button className="sendNotfictaionBtn" type="primary" block>
      Send Notification
    </Button>
  </React.Fragment>
);
