import React from "react";
import { Input, Button } from "antd";

// const { Textarea } = Input;

export const AddCandidatesInner = () => (
  <React.Fragment>
    <Input size="large" placeholder="Name Surname" />
    <Input type="email" size="large" placeholder="Email Address" />
    <Input type="password" size="large" placeholder="Password" />
    <Button className="addCandidateBt" type="primary" block>
      Create Candidate
    </Button>
  </React.Fragment>
);
