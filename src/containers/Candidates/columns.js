import React from "react";
import Icon from "antd/lib/icon";
import { withRouter } from "react-router-dom";
import { Switch } from "antd";

const iconStyle = {
  cursor: "pointer",
  padding: "10px",
  background: "#23B1D3",
  borderRadius: "50%",
  color: "white"
};

const ArrowButton = withRouter(({ history, record }) => {

  return (
    <Icon
      type="right"
      style={iconStyle}
      onClick={() => history.push("/dashboard/candidates/" + record.id)}
    />
  );
});

export const columns = [
  // {
  //   title: "Candidate ID",
  //   dataIndex: "id",
  //   onFilter: (value, record) => record.id.indexOf(value) === 0,
  //   sorter: (a, b) => a.id.length - b.id.length
  //   // sortDirections: ['descend'],
  //   // sorter: (a, b) => a.id - b.id,
  // },

  {
    title: "Email",
    dataIndex: "email",
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => a.email.length - b.email.length
    // sortDirections: ['descend'],
    // sorter: (a, b) => a.id - b.id,
  },
  {
    title: "Talent Email",
    dataIndex: "talentEmail",
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => a.talentEmail.length - b.talentEmail.length
    // sortDirections: ['descend'],
    // sorter: (a, b) => a.id - b.id,
  },
  {
    title: "Name Lastname",
    dataIndex: "name",
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.name.indexOf(value) === 0,
    sorter: (a, b) => a.name.length - b.name.length
    // sortDirections: ['descend'],
  },
  {
    title: "title",
    dataIndex: "title",
    // defaultSortOrder: 'descend',
    onFilter: (value, record) => record.title.indexOf(value) === 0,
    sorter: (a, b) => a.title.length - b.title.length
    // sortDirections: ['descend'],
  },
  {
    title: "Talent Number",
    dataIndex: "candidateVoxNumber",
    onFilter: (value, record) => record.candidateVoxNumber.indexOf(value) === 0,
    sorter: (a, b) => a.id.length - b.candidateVoxNumber.length
    // sortDirections: ['descend'],
    // sorter: (a, b) => a.id - b.id,
  },
  {
    title: "Online Status",
    key: "isOnline",
    dataIndex: "isOnline",
    // onFilter: (value, record) => record.signupDate.indexOf(value) === 0,
    // sorter: (a, b) => a.signupDate.length - b.signupDate.length,
    // sortDirections: ['descend', 'ascend']
    render: (text, record) => {
      return (
        <div style={{ textAlign: "center" }}>
          <Switch
            checked={record.isOnline}
            style={{
              background: record.isOnline ? "#00D157" : "#F44F4F",
              marginRight: 15
            }}
          />
        </div>
      );
    }
  },
  {
    title: "",
    key: "link",
    dataIndex: "link",
    // onFilter: (value, record) => record.signupDate.indexOf(value) === 0,
    // sorter: (a, b) => a.signupDate.length - b.signupDate.length,
    // sortDirections: ['descend', 'ascend']
    render: (text, record) => {
      return <ArrowButton record={record} />;
    }
  }
];
