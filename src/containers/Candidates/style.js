import styled from "styled-components";
// import { palette } from "styled-theme";
// import WithDirection from "../../settings/withDirection";

export const CandidateScreenStyle = styled.div`
  .ant-tabs {
    padding: 0;
  }
  .ant-tabs-bar {
    background: white;
    margin: 0;
  }
  .ant-tabs-tab {
    color: #454545;
    font-size: 14px;
    font-weight: 600;
    line-height: 24px;
  }
  .ant-tabs-ink-bar {
    background-color: #23b1d3;
  }
  .ant-tabs-tab:hover {
    color: #23b1d3 !important;
  }
  .ant-tabs-nav .ant-tabs-tab-active {
    color: #23b1d3;
    font-weight: 600 !important;
  }
  .new-signup-widget {
    border: 0px !important;
    margin: 30px;
  }
  .ant-tabs-content {
    border-radius: 5px 5px 0 0;

    // padding: 30px;
    background: transparent;
  }
`;

export const NewSignUp = styled.div``;
// export default WithDirection(CandidateScreenStyle);
