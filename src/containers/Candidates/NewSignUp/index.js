import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
import { columns } from "../columns";
import { CANDIDATES_QUERY } from "./Schema";
import { Tabs } from "antd";
import Table from "../../../components/Widgets/Table";
import AllCandidates from "../AllCandidates";

const TabPane = Tabs.TabPane;

class NewSignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      tabs: [
        // {
        //   key: "1",
        //   title: "All Candidates",
        //   Component: AllCandidates
        // },
        {
          key: "1",
          title: "New Sign Up",
          Component: AllCandidates
        }
      ]
    };
    this.onChange = this.onChange.bind(this);
    this.renderDataSource = this.renderDataSource.bind(this);
  }

  onChange(pagination, filters, sorter) {
    console.log("params", pagination, filters, sorter);
  }

  renderDataSource(data) {
    if (Array.isArray(data)) {
      return data.map(item => ({
        ...item,
        title: (item.position && item.position.title) || "",
        signupDate: item.createdAt,
        key: item.id
      }));
    }
    return [];
  }

  callback(key) {
    console.log(key);
  }
  render() {
    // const {
    //   data: { candidates, loading }
    // } = this.props;

    return (
      <div>
        <Tabs defaultActiveKey="1" onChange={this.callback} className="_tabs">
          {this.state.tabs.map(({ key, title, Component }) => (
            <TabPane tab={title} key={key} className="_tab">
              <Component columns={columns} />
            </TabPane>
          ))}
        </Tabs>
        {/* <Table
          columns={columns}
          loading={loading}
          dataSource={this.renderDataSource(candidates)}
          onChange={this.onChange}
          isHeader={false}
        /> */}
      </div>
    );
  }
}

export default compose(graphql(CANDIDATES_QUERY))(NewSignUp);
