import gql from "graphql-tag";

export const SIGNIN_WITH_GOOGLE = gql`
  mutation(
    $email: String!
    $password: String
    $serverAuthCode: String!
    $name: String!
  ) {
    signInWithGoogle(
      email: $email
      password: $password
      serverAuthCode: $serverAuthCode
      name: $name
    ) {
      name
      email
      talentEmail
      token
      id
      status
    }
  }
`;
