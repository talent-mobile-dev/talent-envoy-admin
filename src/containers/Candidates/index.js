import React, { Component } from "react";
import { Button, Input, Tabs, Form, notification } from "antd";
import { CandidateScreenStyle } from "./style";

import { ContentHeader } from "../../../src/components/Header";
import AllCandidates from "./AllCandidates";
// import NewSignUp from "./NewSignUp";
import { columns } from "./columns";
import SendNotificationModal from "../../components/Modal";

import GoogleLogin from "react-google-login";
import { graphql } from "react-apollo";
import { compose } from "recompose";
import { SIGNIN_WITH_GOOGLE } from "./Schema";

const TabPane = Tabs.TabPane;
const { TextArea } = Input;

class CandidatesInnerForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      password: ""
    };
  }

  onChangeInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = (googleResponse, resetFields, modalShow) => {
    const { signInWithGoogleMutation } = this.props;
    const { name, email, password } = this.state;
    if (googleResponse) {
      signInWithGoogleMutation({
        variables: {
          name,
          email,
          password,
          serverAuthCode: googleResponse.code
        }
      })
        .then(data => {
          resetFields();
          modalShow(false);

          notification.success({
            message: `New Candidate Created/Updated`,
            description: `${data.data.signInWithGoogle.name} - ${
              data.data.signInWithGoogle.email
            } - TE Mail:  ${data.data.signInWithGoogle.talentEmail}`,
            key: data.id
          });

        })
        .catch(error => {
          notification.error({
            message: `New Candidate Create Error`,
            description: error.message
          });
          console.log(error);
        });
    }
  };

  render() {
    const { getFieldDecorator, validateFields, resetFields } = this.props.form;
    const { modalShow } = this.props;

    return (
      <Form className="login-form">
        <Form.Item>
          {getFieldDecorator("name", {
            rules: [{ required: true, message: "Please input your username!" }]
          })(
            <Input
              size="large"
              placeholder="Name Surname"
              name="name"
              onChange={this.onChangeInput}
            />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator("email", {
            rules: [{ required: true, message: "Please input your email!" }]
          })(
            <Input
              size="large"
              placeholder="Email"
              name="email"
              onChange={this.onChangeInput}
            />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator("password", {
            rules: [{ required: true, message: "Please input your password!" }]
          })(
            <Input
              type="password"
              size="large"
              placeholder="Password"
              name="password"
              onChange={this.onChangeInput}
            />
          )}
        </Form.Item>
        <Form.Item>
          <GoogleLogin
            responseType="code"
            offline={true}
            accessType="offline"
            approvalPrompt="force"
            prompt="consent"
            render={renderProps => (
              <Button
                className="addCandidateBt"
                type="primary"
                block
                onClick={() => {
                  validateFields((err, values) => {
                    if (!err) {
                      renderProps.onClick();
                    }
                  });
                }}
              >
                Sign up with Google
              </Button>
            )}
            scope="https://mail.google.com/"
            clientId="61033850314-o8sto16qn7tus56avvurk8i3dble346h.apps.googleusercontent.com"
            onSuccess={response => {
              this.handleSubmit(response, resetFields, modalShow);
            }}
            onFailure={error => console.log("Sign in failed", error)}
          />
        </Form.Item>
      </Form>
    );
  }
}

const WrappedCandidatesInnerForm = Form.create({ name: "candidate_create" })(
  compose(
    graphql(SIGNIN_WITH_GOOGLE, {
      name: "signInWithGoogleMutation"
    })
  )(CandidatesInnerForm)
);

const NotificationInner = () => (
  <React.Fragment>
    <Button className="addCandidateBtn" type="primary" block>
      Add Candidate
    </Button>
    <br />
    <br />
    <Input size="large" placeholder="Title" />
    <br />
    <br />
    <TextArea rows={4} size="large" placeholder="Description" />
    <br />
    <br />
    <Button className="addCandidateBt" type="primary" block>
      Send Notification
    </Button>
  </React.Fragment>
);

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ShowHideNotificationModal: false,
      selectedModal: "send_notification",
      tabs: [
        {
          key: "1",
          title: "All Candidates",
          Component: AllCandidates,
          totalLength: 0
        },
        {
          key: "2",
          title: "New Sign Up",
          Component: AllCandidates,
          totalLength: 0
        },
        {
          key: "3",
          title: "In Progress",
          Component: AllCandidates,
          totalLength: 0
        },
        {
          key: "4",
          title: "Onboarded",
          Component: AllCandidates,
          totalLength: 0
        }
      ]
    };
    this.callback = this.callback.bind(this);
    this.modalShow = this.modalShow.bind(this);
  }

  callback(key) {
    console.log(key);
  }

  onPressCreateButton = e => {
    e.preventDefault();
  };

  modalShow(modalPara, selectedModal) {
    this.setState({
      ShowHideNotificationModal: modalPara,
      selectedModal: selectedModal || this.state.selectedModal
    });
  }

  render() {
 
    return (
      <CandidateScreenStyle>
        <ContentHeader
          title="Candidates"
          renderPopup={value => this.modalShow(true, value)}
        />
        <SendNotificationModal
          onOk={() => this.modalShow(false)}
          OnCancel={() => this.modalShow(false)}
          visible={this.state.ShowHideNotificationModal}
          className={this.state.selectedModal}
          title={
            this.state.selectedModal === "send__notification"
              ? "Send Notification"
              : "Add Candidates"
          }
        >
          {this.state.selectedModal === "send__notification" ? (
            <NotificationInner />
          ) : (
            <WrappedCandidatesInnerForm
              modalShow={this.modalShow}
              onPressCreateButton={this.onPressCreateButton}
            />
          )}
        </SendNotificationModal>
        <Tabs defaultActiveKey="1" onChange={this.callback} className="_tabs">
          {this.state.tabs.map(
            ({ key, title, Component, totalLength }, index) => (
              <TabPane
                tab={`${title} (${totalLength})`}
                key={key}
                className="_tab"
                forceRender
              >
                <Component
                  columns={columns}
                  render={data => {
                    if (data.length !== totalLength) {
                      this.setState(prevState => {
                        const tabs = [...prevState.tabs];
                        tabs[index].totalLength = data.length;
                        return {
                          tabs
                        };
                      });
                    }
                  }}
                />
              </TabPane>
            )
          )}
        </Tabs>
      </CandidateScreenStyle>
    );
  }
}
