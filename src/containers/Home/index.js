import React, { Component } from "react";
import LayoutContentWrapper from "../components/utility/layoutWrapper";
import LayoutContent from "../components/utility/layoutContent";
import { Row, Col } from "antd";
import CardWidget from "../components/Widgets/card";
import NewSignup from "./Widgets/new-signup/new-signup";
import DashboardStyle from "./dashboard-style";
import NotificationWidget from "../components/Widgets/notifications";

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CardWidgetData: {
        totalCandidate: "3124",
        totalIos: "3124",
        totalAndroid: "603"
      }
    };
  }

  render() {
    return (
      <DashboardStyle>
        <LayoutContentWrapper>
          <LayoutContent>
            <div>
              <Row>
                <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                  <Row className="pr-35">
                    <Col
                      xs={{ span: 24 }}
                      sm={{ span: 24 }}
                      md={{ span: 24 }}
                      lg={{ span: 24 }}
                    >
                      <Row>
                        <Col
                          xs={{ span: 24 }}
                          sm={{ span: 12 }}
                          md={12}
                          lg={12}
                        >
                          <div className="mr-17">
                            <CardWidget
                              bgcolor="#5B3AEA"
                              number="22"
                              text="unread messages"
                              // iconType="right"
                            />
                          </div>
                        </Col>

                        <Col
                          xs={{ span: 24 }}
                          sm={{ span: 12 }}
                          md={{ span: 12 }}
                          lg={{ span: 12 }}
                        >
                          <div className="ml-17">
                            <CardWidget
                              bgcolor="#FA6C2B"
                              number="20"
                              text="Today New Sign Up"
                              iconType="right"
                            />
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col
                          xs={{ span: 24 }}
                          sm={{ span: 12 }}
                          md={12}
                          lg={12}
                        >
                          <div className="mr-17">
                            <CardWidget
                              bgcolor="#9339E9"
                              number="10"
                              text="Today In Progress"
                              iconType="right"
                            />
                          </div>
                        </Col>
                        <Col
                          xs={{ span: 24 }}
                          sm={{ span: 12 }}
                          md={{ span: 12 }}
                          lg={{ span: 12 }}
                        >
                          <div className="ml-17">
                            <CardWidget
                              bgcolor="#39CEE8"
                              number="70"
                              text="Today Onboarded"
                              iconType="right"
                            />
                          </div>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  <Row className="pr-35">
                    <Col
                      xs={{ span: 24 }}
                      sm={{ span: 24 }}
                      md={{ span: 24 }}
                      lg={{ span: 24 }}
                    >
                      <ul className="_totalCounter">
                        <li>
                          <CardWidget
                            bgcolor=""
                            number={this.state.CardWidgetData.totalCandidate}
                            text="Total Candidate"
                          />
                        </li>
                        <li>
                          <CardWidget
                            bgcolor=""
                            number={this.state.CardWidgetData.totalIos}
                            text="Total iOS"
                          />
                        </li>
                        <li>
                          <CardWidget
                            bgcolor=""
                            number={this.state.CardWidgetData.totalAndroid}
                            text="Total Android"
                          />
                        </li>
                      </ul>
                    </Col>
                  </Row>
                  <Row className="pr-35">
                    <Col
                      xs={{ span: 24 }}
                      sm={{ span: 24 }}
                      md={{ span: 24 }}
                      lg={{ span: 24 }}
                    >
                      <NotificationWidget />
                    </Col>
                  </Row>
                </Col>
                <Col
                  xs={24}
                  sm={24}
                  md={{ span: 16 }}
                  lg={{ span: 16 }}
                  xl={{ span: 16 }}
                >
                  <NewSignup />
                </Col>
              </Row>
            </div>
          </LayoutContent>
        </LayoutContentWrapper>
      </DashboardStyle>
    );
  }
}
