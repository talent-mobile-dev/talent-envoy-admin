import styled from "styled-components";
// import { palette } from 'styled-theme';
import WithDirection from "../settings/withDirection";

const DashboardStyle = styled.div`
  padding: 0px 15px;
  // padding-right: 25px;

  ._totalCounter {
    padding: 0;
    border-radius: 5px;
    background-color: #0e4bd8 !important;
    height: 126px;
    li:nth-child(2) .isoCardWidget {
      border-right: 1px solid #1b5aec;
      border-left: 1px solid #1b5aec;
    }
    li {
      list-style-type: none;
      float: left;
      width: 33.1%;
      .isoCardWidget {
        margin: 0;
        border-radius: 0;
        background-color: transparent !important;
      }
    }
  }
`;

export default WithDirection(DashboardStyle);
