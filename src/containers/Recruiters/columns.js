import React from "react";
import Icon from "antd/lib/icon";
import { withRouter } from "react-router-dom";

const iconStyle = {
  cursor: "pointer",
  padding: "10px",
  background: "#23B1D3",
  borderRadius: "50%",
  color: "white"
};

const ArrowButton = withRouter(({ history, record }) => {

  return (
    <Icon
      type="right"
      style={iconStyle}
      onClick={() => history.push("candidates/" + record.id)}
    />
  );
});

export const columns = [
  {
    title: "id",
    dataIndex: "id",
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => a.id.length - b.id.length
    // sortDirections: ['descend'],
    // sorter: (a, b) => a.id - b.id,
  },
  {
    title: "Name Lastname",
    dataIndex: "name",
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.name.indexOf(value) === 0,
    sorter: (a, b) => a.name.length - b.name.length
    // sortDirections: ['descend'],
  },
  {
    title: "company",
    dataIndex: "company",
    // defaultSortOrder: 'descend',
    onFilter: (value, record) => record.company.indexOf(value) === 0,
    sorter: (a, b) => a.company.length - b.company.length
    // sortDirections: ['descend'],
  },
  {
    title: "mail address",
    dataIndex: "mail",

    // filterMultiple: false,
    onFilter: (value, record) => record.mail.indexOf(value) === 0,
    sorter: (a, b) => new Date(a.mail) - new Date(b.mail)
    // sortDirections: ['descend'],
  },
  {
    title: "phone number",
    dataIndex: "phone",

    // filterMultiple: false,
    onFilter: (value, record) => record.phone.indexOf(value) === 0,
    sorter: (a, b) => new Date(a.phone) - new Date(b.phone)
    // sortDirections: ['descend'],
  }
  // {
  //   title: "link",
  //   key: "link",
  //   dataIndex: "link",
  //   // onFilter: (value, record) => record.signupDate.indexOf(value) === 0,
  //   // sorter: (a, b) => a.signupDate.length - b.signupDate.length,
  //   // sortDirections: ['descend', 'ascend']
  //   render: (text, record) => {
  //     return <ArrowButton record={record} />;
  //   }
  // }
];
