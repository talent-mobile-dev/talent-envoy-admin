import React, { Component } from "react";
import { Table, Button, Input, Tabs, Form } from "antd";
import { RecruiterScreenStyle } from "./style";

import { ContentHeader } from "../../../src/components/Header/recruiter-header";
import AllRecruiters from "./AllRecruiters";

import { columns } from "./columns";
import SendNotificationModal from "../../components/Modal";
import GoogleLogin from "react-google-login";
import { graphql } from "react-apollo";
import { compose } from "recompose";

const { TextArea } = Input;

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = { filteredInfo: null,
      sortedInfo: null};
  }
  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  }

  render() {

    return (
      <RecruiterScreenStyle>
        <ContentHeader title="Recruiters" />
      
        <AllRecruiters
                  columns={columns}
                />

        </RecruiterScreenStyle>
    );
  }
}
