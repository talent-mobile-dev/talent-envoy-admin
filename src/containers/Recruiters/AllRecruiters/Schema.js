import gql from "graphql-tag";

export const RECRUITERS_QUERY = gql`
  query recruiters {
    recruiters {
      id
      email
      createdAt
      # provider {
      #     google
      #     default
      # }
      name
      profileUrl
      # status
      # fcmToken
      # fcmTokenUpdatedAt
      # businessHours
      # notAvailability
      # callLimitationPerWeek
      # isOnline
      #recruiterNumber
    }
  }
`;
