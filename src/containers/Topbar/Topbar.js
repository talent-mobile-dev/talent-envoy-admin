import React, { Component } from "react";
import styled from "styled-components";
import { connect } from "react-redux";
import { Layout, Icon } from "antd";
import appActions from "../../redux/app/actions";
import TopbarUser from "./topbarUser";
import TopbarWrapper from "./topbar.style";
import themes from "../../settings/themes";
import { themeConfig } from "../../settings";

const { Header } = Layout;
const { toggleCollapsed } = appActions;
const customizedTheme = themes[themeConfig.theme];

const TopbarToggle = styled.button`
  position: absolute;
  height: 90px;
  width: 81px;
  background: #0033bd;
  z-index: 9999;
  left: 0px;
  font-size: 42px;
  border: none;
  color: white;
  border-right: 1px solid white;
  &:focus {
    outline: none;
  }
  @media only screen and (max-width: 767px) {
    position: relative;
    width: 100%;
    text-align: right;
    height: 104px;
    padding-right: 25px;
    &:hover {
      left: 0;
    }
  }
`;
class Topbar extends Component {
  render() {
    const { toggleCollapsed } = this.props;
    const collapsed = this.props.collapsed && !this.props.openDrawer;
    const styling = {
      background: customizedTheme.backgroundColor,
      position: "abosulute",
      width: "10px",
      left: 0,
      height: 90
    };

    return (
      <TopbarToggle
        className={
          collapsed ? "triggerBtn menuCollapsed" : "triggerBtn menuOpen"
        }
        style={{ color: customizedTheme.textColor }}
        onClick={toggleCollapsed}
      >
        <Icon type="menu-fold" />
      </TopbarToggle>
    );
    // return (
    //   <TopbarWrapper>
    //     <Header
    //       style={styling}
    //       className={
    //         collapsed ? "isomorphicTopbar collapsed" : "isomorphicTopbar"
    //       }
    //     >
    //       <div className="isoLeft">
    //         <button
    //           className={
    //             collapsed ? "triggerBtn menuCollapsed" : "triggerBtn menuOpen"
    //           }
    //           style={{ color: customizedTheme.textColor }}
    //           onClick={toggleCollapsed}
    //         >
    //           <Icon type="menu-fold" />
    //         </button>
    //       </div>
    //     </Header>
    //   </TopbarWrapper>
    // <TopbarWrapper>
    //   <Header
    //     style={styling}
    //     className={
    //       collapsed ? "isomorphicTopbar collapsed" : "isomorphicTopbar"
    //     }
    //   >
    //     <div className="isoLeft">
    //       <button
    //         className={
    //           collapsed ? "triggerBtn menuCollapsed" : "triggerBtn menuOpen"
    //         }
    //         style={{ color: customizedTheme.textColor }}
    //         onClick={toggleCollapsed}
    //       >
    //         <Icon type="menu-fold" />
    //       </button>
    //     </div>

    //     <ul className="isoRight">
    //       <li
    //         onClick={() => this.setState({ selectedItem: "user" })}
    //         className="isoUser"
    //       >
    //         <TopbarUser />
    //       </li>
    //     </ul>
    //   </Header>
    // </TopbarWrapper>
    // );
  }
}

export default connect(
  state => ({
    ...state.App
  }),
  { toggleCollapsed }
)(Topbar);
