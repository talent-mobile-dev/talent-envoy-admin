import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { Button, Modal } from "antd";
import Icon from "antd/lib/icon";
import ChatRooms from "./chatrooms";
import Messages from "./messages";
import ComposeMessage from "./composMessage";
import ViewProfile from "../../components/chat/viewProfile";
import Loader from "../../components/utility/loader";
import HelperText from "../../components/utility/helper-text";
import Scrollbars from "../../components/utility/customScrollBar.js";
import avatar from "../../image/avatar.jpeg";

import {
  Mstyle,
  ChatWindow,
  ChatBox,
  ToggleViewProfile,
  Conversation
} from "./message.style";

import { timeDifference } from "../../helpers/utility";

import actions from "../../redux/chat/actions";

class DesktopView extends Component {
  state = {
    profile: {},
    viewProfile: false,
    message: false,
    conversationId: "",
    newConversationText: "",
    newConversationModalVisible: false,
    isNewRoom: true
  };

  setNewConversationModalVisible(newConversationModalVisible) {
    this.setState({ newConversationModalVisible: newConversationModalVisible });
  }

  componentDidMount() {
    const { chatInit, redirect, setRedirect } = this.props;

    if (!redirect) {
      chatInit();
    } else {
      setRedirect(false);
      this.setState({ message: false });
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedChatRoom)
      if (this.props.selectedChatRoom.id != prevProps.selectedChatRoom.id)
        this.setState({ isNewRoom: true });


    const { redirect } = prevProps;
    if (redirect) {
      this.props.setRedirect(false);
      this.setState({ message: false });
    }
  }

  viewProfile = profile => {
    this.setState({ profile, viewProfile: true });
  };

  closeProfile = () => {
    this.setState({ viewProfile: false });
  };

  openConversation = conversation => {
    const { getMessage, removeNewMessageConv } = this.props;
    removeNewMessageConv(conversation.id);
    getMessage(conversation.id);

    this.setState({
      message: true,
      conversationId: conversation.id
    });
  };
  chatInitiate = chatRoom => {
    const { initMessage } = this.props;
    const { newConversationText } = this.state;
    const obj = {
      newConversationText,
      chatRoom,
      selected: true
    };
    console.log(obj);
    initMessage(obj);
  };

  render() {
    const {
      loading,
      selectedChatRoom,
      conversations,
      newMessages
    } = this.props;
    const { message, conversationId } = this.state;
    if (loading) {
      return <Loader />;
    }

    const conversation = (conversation, index) => {
      const { conversation_message } = conversation;
      const notified = _.findIndex(newMessages, message => {
        return message.conv_id === conversation.id;
      });

      const style = {
        backgroundColor: notified > -1 ? "#ffff00" : "rgba(0,0,0,0)"
      };

      if (this.state.isNewRoom) {
        this.setState({ conversationId: conversations[0].id });
        this.props.getMessage(conversations[0].id);
        this.setState({ isNewRoom: false });
      }

      if (!message) {
        return (
          <div
            key={index.toString()}
            style={{
              backgroundColor:
                this.props.selectedConversation == conversation.id
                  ? "#f4f7fc"
                  : ""
            }}
          >
            <Conversation
              onClick={() => {
                this.selectedConversationId = conversation.id;
                this.setState({
                  conversationId: conversation.id
                });
                this.props.getMessage(conversation.id);
              }}
            >
              <div>
                <div
                  style={{
                    fontWeight: 600
                  }}
                >
                  {conversation_message.body.length > 30
                    ? conversation_message.body
                        .replace(/<[^>]*>/g, "")
                        .substr(0, 30) + "..."
                    : conversation_message.body
                        .replace(/<[^>]*>/g, "")
                        .substr(0, 30)}
                </div>
                <div>{timeDifference(conversation.updated_at * 1000)}</div>
              </div>
            </Conversation>
          </div>
        );
      } else {
        return "";
      }
    };

    const selectedChatRoomConversations = (
      <div id="Mdiv3">
        <Scrollbars>
          {conversations.length === 0 ? (
            <HelperText text="No Conversation" className="messageHelperText" />
          ) : (
            conversations.map(conversation)
          )}
        </Scrollbars>
        {this.state.viewProfile !== false ? (
          <ViewProfile
            user={selectedChatRoom.otherUserInfo}
            toggleViewProfile={() => this.closeProfile()}
            viewProfile={this.state.profile}
          />
        ) : (
          ""
        )}

        <Button
          style={{
            backgroundColor: "#f4f7fc",
            width: "100%",
            height: 25,
            fontSize: 12,
            position: "absolute",
            margin: 0,
            bottom: 0
          }}
          onClick={() => this.setNewConversationModalVisible(true)}
        >
          Start New Conversation
        </Button>
        <Modal
          title="New Conversation"
          centered
          visible={this.state.newConversationModalVisible}
          onOk={() => this.setNewConversationModalVisible(false)}
          onCancel={() => this.setNewConversationModalVisible(false)}
          footer={[
            <Button
              key="back"
              onClick={() => this.setNewConversationModalVisible(false)}
            >
              Cancel
            </Button>,
            <Button
              key="submit"
              type="primary"
              loading={loading}
              onClick={() => {
                this.chatInitiate(selectedChatRoom);
                this.props.clearMessages();
                this.setNewConversationModalVisible(false);
              }}
            >
              Start Conversation with Message
            </Button>
          ]}
        >
          <textarea
            value={this.state.newConversationText}
            onChange={e =>
              this.setState({ newConversationText: e.target.value })
            }
            style={{
              width: "100%",
              marginTop: "20px",
              fontSize: "14px",
              fontWeight: "300",
              border: "1px solid #EEEEEE",
              borderRadius: "5px",
              background: "#F9F9F9"
            }}
            rows="4"
            cols="50"
            placeholder="Type your message."
          />
        </Modal>
      </div>
    );

    const upperBanner = (
      <div className="banner-box">
        <div className="banner-image">
          {selectedChatRoom && (
            <img
              alt="#"
              style={{
                width: 40,
                minWidth: 40,
                height: 40,
                borderRadius: "50%",
                padding: 0,
                marginLeft: 0,
                marginRight: 0
              }}
              src={
                selectedChatRoom.otherUserInfo.profileImageUrl
                  ? selectedChatRoom.otherUserInfo.profileImageUrl
                  : avatar
              }
            />
          )}
        </div>
        <div className="banner-name">
          <span onClick={() => this.viewProfile(selectedChatRoom)}>
            {selectedChatRoom
              ? selectedChatRoom.otherUserInfo.name.toUpperCase()
              : ""}
          </span>
        </div>

        <div className="banner-location-phone">
          <div>
            <span style={{ color: "#23B1D3" }}>Location: </span>
            <span>
              {selectedChatRoom ? selectedChatRoom.location.city_name : " "}
            </span>
          </div>

          <div>
            <span style={{ color: "#23B1D3" }}>Phone&nbsp;&nbsp;&nbsp;: </span>
            <span>
              {selectedChatRoom ? selectedChatRoom.otherUserInfo.phone : ""}
            </span>
          </div>
        </div>

        <div className="banner-email-status">
          <div>
            <span style={{ color: "#23B1D3" }}>Email&nbsp;&nbsp;&nbsp;: </span>
            <span>{selectedChatRoom ? selectedChatRoom.email : " "}</span>
          </div>

          <div>
            <span style={{ color: "#23B1D3" }}>Status&nbsp;&nbsp;: </span>
            <span>Onboarded</span>
          </div>
        </div>
      </div>
    );

    const messageComposeBox = (
      <div id="Mdiv4">
        {conversations.length === 0 ? (
          <HelperText text="No Conversation" className="messageHelperText" />
        ) : (
          <span>
            <Messages conversationId={conversationId} />
            <ComposeMessage
              autosize={{ minRows: 2, maxRows: 6 }}
              conversationId={conversationId}
            />
          </span>
        )}
      </div>
    );

    return (
      <Mstyle>
        <div id="Mcont">
          <div id="Mdiv1">
            <ChatRooms />
          </div>
          <div id="Mcont2">
            <div id="Mdiv2">{upperBanner}</div>
            <div id="Mcont3">
              {selectedChatRoomConversations}
              {messageComposeBox}
            </div>
          </div>
        </div>
      </Mstyle>
    );
  }
}
function mapStateToProps(state) {

  return state.Chat;
}
export default connect(
  mapStateToProps,
  actions
)(DesktopView);
