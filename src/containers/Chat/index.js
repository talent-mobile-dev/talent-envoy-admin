import React, { Component } from "react";
import { connect } from "react-redux";
import DesktopView from "./desktopView";
import MobileView from "./mobileView";
import { ChatViewWrapper } from "./message.style";
import { ContentHeader } from "../../../src/components/Header/chat-header";

class Chat extends Component {
  render() {
    const { view, height } = this.props;
    const ChatView = view === "MobileView" ? MobileView : DesktopView;
    return (
      <div>
        <ContentHeader title="Chat" />

        <ChatViewWrapper
          style={{
            height: view === "MobileView" ? height - 108 : height - 138
          }}
        >
          <ChatView height={height} view={view} {...this.props} />
        </ChatViewWrapper>
      </div>
    );
  }
}
export default connect(state => ({
  ...state.App,
  height: state.App.height
}))(Chat);
