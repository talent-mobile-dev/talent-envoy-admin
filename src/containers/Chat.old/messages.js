import React, { Component } from "react";
import { connect } from "react-redux";
import { timeDifference } from "../../helpers/utility";
import { MessageSingle, MessageChatWrapper } from "./message.style";
import { adminInfo, otherAdminProfile, botProfile } from "../../settings";
import actions from "../../redux/chat/actions";
import avatar from "../../image/avatar.jpeg";

class Messages extends Component {
  scrollToBottom = () => {
    const messageChat = document.getElementById("messageChat");
    messageChat.scrollTop = messageChat.scrollHeight;
  };
  componentDidMount() {
    this.scrollToBottom();
  }
  componentDidUpdate() {
    this.scrollToBottom();
  }
  render() {
    const {
      selectedChatRoom,
      messages,
      toggleViewProfile,
      toggleMobileProfile
    } = this.props;

    const renderMessage = message => {
      const isUser = selectedChatRoom.id === message.sender.id;
      const messageUser = isUser
        ? selectedChatRoom.otherUserInfo
        : message.sender;

      if (!isUser) {
        return (
          <MessageSingle className="loggedUser" key={message.messageTime}>
            <div className={"messageContent isUser"}>
              {message.text ? (
                <div
                  dangerouslySetInnerHTML={{ __html: message.text }}
                  className="messageContentText"
                />
              ) : message.part_type === "open" ? (
                <div>
                  <i>This message was opened</i>
                </div>
              ) : (
                <div>
                  <i>This message was closed</i>
                </div>
              )}
              <div className="messageTime">
                <p>{timeDifference(message.messageTime * 1000)}</p>
              </div>
            </div>
            <div className="messageGravatar">
              {message.sender.type === "admin" && (
                <img
                  alt="#"
                  src={
                    message.sender.id === adminInfo.id
                      ? adminInfo.avatar_url
                      : otherAdminProfile
                  }
                  onClick={() => {
                    toggleMobileProfile(true);
                    toggleViewProfile(messageUser);
                  }}
                />
              )}
              {message.sender.type === "bot" && (
                <img
                  alt="#"
                  src={botProfile}
                  onClick={() => {
                    toggleMobileProfile(true);
                    toggleViewProfile(messageUser);
                  }}
                />
              )}
            </div>
          </MessageSingle>
        );
      } else {
        return (
          <MessageSingle key={message.messageTime}>
            <div className="messageGravatar">
              <img
                alt="#"
                src={
                  messageUser.profileImageUrl
                    ? messageUser.profileImageUrl
                    : avatar
                }
                onClick={() => {
                  toggleMobileProfile(true);
                  toggleViewProfile(messageUser);
                }}
              />
            </div>
            <div className="messageContent notUser">
              <div
                dangerouslySetInnerHTML={{ __html: message.text }}
                className="messageContentText"
              />
              <div className="messageTime">
                <p>{timeDifference(message.messageTime * 1000)}</p>
              </div>
            </div>
          </MessageSingle>
        );
      }
    };
    return (
      <MessageChatWrapper id="messageChat">
        {messages.map(renderMessage)}
      </MessageChatWrapper>
    );
  }
}
function mapStateToProps(state) {
  
  const { selectedChatRoom, messages } = state.Chat;
  return {
    selectedChatRoom,
    messages
  };
}
export default connect(
  mapStateToProps,
  actions
)(Messages);
