import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { Button, Modal } from "antd";
import Icon from "antd/lib/icon";
import ChatRooms from "./chatrooms";
import Messages from "./messages";
import ComposeMessage from "./composMessage";
import ViewProfile from "../../components/chat/viewProfile";
import Loader from "../../components/utility/loader";
import HelperText from "../../components/utility/helper-text";
import Scrollbars from "../../components/utility/customScrollBar.js";
import avatar from "../../image/avatar.jpeg";

import {
  ChatWindow,
  ChatBox,
  ToggleViewProfile,
  Conversation
} from "./message.style";

import { timeDifference } from "../../helpers/utility";

import actions from "../../redux/chat/actions";

class DesktopView extends Component {
  state = {
    profile: {},
    viewProfile: false,
    message: false,
    conversationId: "",
    newConversationText: "",
    newConversationModalVisible: false
  };

  setNewConversationModalVisible(newConversationModalVisible) {
    this.setState({ newConversationModalVisible: newConversationModalVisible });
  }

  componentDidMount() {
    const { chatInit, redirect, setRedirect } = this.props;
    if (!redirect) {
      chatInit();
    } else {
      setRedirect(false);
      this.setState({ message: false });
    }
  }

  componentDidUpdate(prevProps) {
    const { redirect } = prevProps;
    if (redirect) {
      this.props.setRedirect(false);
      this.setState({ message: false });
    }
  }

  viewProfile = profile => {
    this.setState({ profile, viewProfile: true });
  };

  closeProfile = () => {
    this.setState({ viewProfile: false });
  };

  openConversation = conversation => {
    const { getMessage, removeNewMessageConv } = this.props;
    removeNewMessageConv(conversation.id);
    getMessage(conversation.id);

    this.setState({
      message: true,
      conversationId: conversation.id
    });
  };
  chatInitiate = chatRoom => {
    const { initMessage } = this.props;
    const { newConversationText } = this.state;
    const obj = {
      newConversationText,
      chatRoom,
      selected: true
    };
    console.log(obj);
    initMessage(obj);
  };

  render() {
    const {
      loading,
      selectedChatRoom,
      conversations,
      newMessages
    } = this.props;
    const { message, conversationId } = this.state;
    if (loading) {
      return <Loader />;
    }
    const conversation = (conversation, index) => {
      const { conversation_message } = conversation;
      const notified = _.findIndex(newMessages, message => {
        return message.conv_id === conversation.id;
      });
      // console.log("notified======>", notified);
      const style = {
        backgroundColor: notified > -1 ? "#ffff00" : "rgba(0,0,0,0)"
      };

      if (!message) {
        return (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginBottom: 15
            }}
            key={index.toString()}
          >
            <Conversation
              onClick={() => this.openConversation(conversation)}
              style={style}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <h3>
                  Conversation with {selectedChatRoom.otherUserInfo.name}
                  {/* {conversation_message.delivered_as === "customer_initiated"
                    ? "Candidate replied to this conversation"
                    : "You replied to this conversation"} */}
                </h3>
                <span> {timeDifference(conversation.updated_at * 1000)}</span>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: conversation_message.body
                  }}
                />
                <div>
                  <span>Status: </span>
                  <span
                    style={
                      conversation.state === "open"
                        ? { color: "green" }
                        : { color: "red" }
                    }
                  >
                    {conversation.state.toUpperCase()}
                  </span>
                </div>
              </div>
            </Conversation>
          </div>
        );
      } else {
        return "";
      }
    };
    if (!message) {
      return (
        <ChatWindow className="ChatWindow">
          <ChatRooms closeMessenger={() => this.setState({ message: false })} />
          <ChatBox style={{ height: "100%" }}>
            {selectedChatRoom && (
              <ToggleViewProfile>
                <span onClick={() => this.viewProfile(selectedChatRoom)}>
                  {selectedChatRoom.otherUserInfo.name}
                </span>
                <Button
                  type="primary"
                  style={{ marginRight: 15 }}
                  onClick={() => this.setNewConversationModalVisible(true)}
                >
                  Initiate Conversation
                </Button>
                <Modal
                  title="New Conversation"
                  centered
                  visible={this.state.newConversationModalVisible}
                  onOk={() => this.setNewConversationModalVisible(false)}
                  onCancel={() => this.setNewConversationModalVisible(false)}
                  footer={[
                    <Button
                      key="back"
                      onClick={() => this.setNewConversationModalVisible(false)}
                    >
                      Cancel
                    </Button>,
                    <Button
                      key="submit"
                      type="primary"
                      loading={loading}
                      onClick={() => this.chatInitiate(selectedChatRoom)}
                    >
                      Start Conversation with Message
                    </Button>
                  ]}
                >
                  {/* {this.selectOptions()} */}
                  <textarea
                    value={this.state.newConversationText}
                    onChange={e =>
                      this.setState({ newConversationText: e.target.value })
                    }
                    style={{
                      width: "100%",
                      marginTop: "20px",
                      fontSize: "14px",
                      fontWeight: "300",
                      border: "1px solid #EEEEEE",
                      borderRadius: "5px",
                      background: "#F9F9F9"
                    }}
                    rows="4"
                    cols="50"
                    placeholder="Type your message."
                  />
                </Modal>
              </ToggleViewProfile>
            )}
            <Scrollbars>
              {conversations.length === 0 ? (
                <HelperText
                  text="No Conversation"
                  className="messageHelperText"
                />
              ) : (
                conversations.map(conversation)
              )}
            </Scrollbars>
          </ChatBox>
          {this.state.viewProfile !== false ? (
            <ViewProfile
              user={selectedChatRoom.otherUserInfo}
              toggleViewProfile={() => this.closeProfile()}
              viewProfile={this.state.profile}
              chatInitiate={() => this.chatInitiate(selectedChatRoom)}
            />
          ) : (
            ""
          )}
        </ChatWindow>
      );
    } else {
      return (
        <ChatWindow className="ChatWindow">
          <ChatRooms closeMessenger={() => this.setState({ message: false })} />
          <ChatBox style={{ height: "100%" }}>
            {selectedChatRoom && (
              <ToggleViewProfile>
                <span onClick={() => this.viewProfile(selectedChatRoom)}>
                  {selectedChatRoom.otherUserInfo.name}
                </span>
              </ToggleViewProfile>
            )}
            <Messages conversationId={conversationId} />
            <ComposeMessage
              autosize={{ minRows: 2, maxRows: 6 }}
              conversationId={conversationId}
            />
          </ChatBox>
          {this.state.viewProfile !== false ? (
            <ViewProfile
              user={selectedChatRoom.otherUserInfo}
              toggleViewProfile={() => this.closeProfile()}
              viewProfile={this.state.profile}
              chatInitiate={() => this.chatInitiate(selectedChatRoom)}
            />
          ) : (
            ""
          )}
        </ChatWindow>
      );
    }
  }
}
function mapStateToProps(state) {
  return state.Chat;
}
export default connect(
  mapStateToProps,
  actions
)(DesktopView);
