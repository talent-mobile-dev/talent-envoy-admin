import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
// import Input from "../../components/uielements/input";
// import AddNewUser from "./addNewUser";
import Scrollbars from "../../components/utility/customScrollBar.js";
import actions from "../../redux/chat/actions";
import HelperText from "../../components/utility/helper-text";
import {
  UserListsWrapper,
  UserLists,
  SidebarSearchBox,
  Input,
  ChatSidebar
} from "./message.style";

import avatar from "../../image/avatar.jpeg";

const getTime = timeStamp => {
  const date = new Date(timeStamp);
  const Month = date.getMonth() + 1;
  const Year = date.getFullYear();
  const d = date.getDate();
  return d + "/" + Month + "/" + Year;
};
class ChatRooms extends Component {
  state = {
    value: "",
    searchedChatRooms: this.props.chatRooms
  };
  componentWillReceiveProps(nextProps) {
    this.setState({ value: "", searchedChatRooms: nextProps.chatRooms });
  }
  onSearch = event => {
    const value = event.target.value;
    const searchedChatRooms = value ? [] : this.props.chatRooms;
    if (value) {
      this.props.chatRooms.forEach(chatRoom => {
        if (
          chatRoom.otherUserInfo.name
            .toLowerCase()
            .includes(value.toLowerCase())
        ) {
          searchedChatRooms.push(chatRoom);
        }
      });
    }
    this.setState({ value, searchedChatRooms });
  };
  render() {
    const {
      setSelectedChatroom,
      selectedChatRoom,
      toggleMobileList,
      newMessages
    } = this.props;
    const { value, searchedChatRooms } = this.state;
    const singleChatRoom = (chatRoom, index) => {
      const { otherUserInfo, lastMessage, lastMessageTime } = chatRoom;
      const { name, profileImageUrl } = otherUserInfo;
      if (!name) {
        return;
      }
      const selected = selectedChatRoom.id === chatRoom.id;
      const notified = _.findIndex(newMessages, message => {
        return message.room_id === chatRoom.id;
      });
      const style = {
        background: selected
          ? "#f7f7f7"
          : notified > -1
          ? "#ffff00"
          : "rgba(0,0,0,0)"
      };
      const selectChatroom = event => {
        event.stopPropagation();
        this.props.closeMessenger();
        if (!selected) {
          setSelectedChatroom(chatRoom);
        }
        if (toggleMobileList) {
          toggleMobileList(false);
        }
      };
      return (
        <UserLists key={index} style={style} onClick={selectChatroom}>
          <div className="userListsGravatar">
            <img
              alt="#"
              style={{ width: 45, height: 45 }}
              src={profileImageUrl ? profileImageUrl : avatar}
            />
          </div>
          <div className="userListsContent">
            <h4>{name}</h4>
            <div className="chatExcerpt">
              <p>{lastMessage}</p>
              <span className="userListsTime">
                {getTime(lastMessageTime * 1000)}
              </span>
            </div>
          </div>
        </UserLists>
      );
    };
    return (
      <ChatSidebar>
        <SidebarSearchBox>
          <Input
            value={value}
            onChange={this.onSearch}
            placeholder="Search Contact"
          />
        </SidebarSearchBox>
        <UserListsWrapper
          style={{
            position: "relative",
            height: "100%",
            width: "100%",
            minWidth: 270,
            overflow: "hidden",
            display: "inline-flex",
            padding: 0
          }}
        >
          {" "}
          <Scrollbars>
            {searchedChatRooms.length === 0 ? (
              <HelperText
                text="No Conversation"
                className="messageHelperText"
              />
            ) : (
              searchedChatRooms.map(singleChatRoom)
            )}
          </Scrollbars>
        </UserListsWrapper>
      </ChatSidebar>
    );
  }
}
function mapStateToProps(state) {
  const {
    users,
    chatRooms,
    openCompose,
    selectedChatRoom,
    newMessages
  } = state.Chat;
  return {
    users,
    newMessages,
    chatRooms: chatRooms.filter(chatRoom => chatRoom.lastMessageTime > 0),
    selectedChatRoom: selectedChatRoom,
    openCompose
  };
}
export default connect(
  mapStateToProps,
  actions
)(ChatRooms);
