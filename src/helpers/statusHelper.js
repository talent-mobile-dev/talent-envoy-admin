const getStatusNameFromId = statusId => {
  let statusDesc = "";
  switch (statusId) {
    case "0":
      statusDesc = "New Sign Up — Onboarding Welcome";
      break;
    case "1":
      statusDesc = "Onboarding — Waiting First Review";
      break;
    case "1_1":
      statusDesc = "Onboarding — Waiting Update Resume";
      break;
    case "1_2":
      statusDesc = "Onboarding — Waiting Additional Questions";
      break;
    case "1_3":
      statusDesc = "Unfortunately Not Continue";
      break;
    case "1_4":
      statusDesc = "Unfortunately Not Continue";
      break;
    case "2":
      statusDesc = "Onboarding - Waiting QA - Start";
      break;
    case "2_1":
      statusDesc = "Onboarding - Waiting QA";
      break;
    case "2_2":
      statusDesc = "Onboarding - Waiting Missing QA";
      break;
    case "3":
      statusDesc = "Onboarding - Waiting Second Review";
      break;
    case "3_1":
      statusDesc = "Onboarded - Set The Batch Time";
      break;
    case "3_2":
      statusDesc = "Onboarded - Waiting Batch";
      break;
    case "4":
      statusDesc = "Onboarded - On The Batch";
      break;
    default:
      statusDesc = "Status Not Found";
      break;
  }

  console.log(statusDesc);
  return statusDesc;
};

export { getStatusNameFromId };
