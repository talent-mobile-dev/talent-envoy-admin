import logo from '../image/logo.png'
import iconSite from '../image/siteIcon.png'

export default {
  // apiUrl: "http://192.168.1.110:8008/api/v1",
  apiUrl: "https://talent-server-app-staging.herokuapp.com/api/v1/chat",
  // wsUrl: "ws://192.168.1.110:8008"
  wsUrl: "ws://talent-server-app-staging.herokuapp.com/"
};

const message = {
  message: "How can I help you?"
}

const adminInfo = {
  name: "Talent Envoy",
  id: 3021805,
  avatar_url:
    "https://static.intercomassets.com/avatars/3021805/square_128/ic_launcher_round-1552111465.png?1552111465"
};

const otherAdminProfile =
  "https://static.intercomassets.com/assets/default-avatars/admins/128-da5b0f4e5162b0f45cba5a162df89b82eaddb92c5f5ae86e18629c8656aa3365.png";

const botProfile =
  "https://static.intercomassets.com/assets/default-avatars/operator/128-10ee062f48e0940906c058aecb8a44c7a02acfc5e35d0fc9a30981611ace0c75.png";

const siteConfig = {
  siteLogo: logo,
  siteIcon: iconSite,
  siteName: 'Talent Envoy',
  // siteIcon: 'ion-heart',
  footerText: 'Talent Envoy - © Copyright 2019',
};
const themeConfig = {
  topbar: 'themedefault',
  sidebar: 'themedefault',
  layout: 'themedefault',
  theme: 'themedefault',
};
const language = 'english';

const jwtConfig = {
  fetchUrl: '/api/',
  secretKey: 'secretKey',
};

export { 
  siteConfig, 
  language, 
  themeConfig, 
  jwtConfig, 
  adminInfo,
  otherAdminProfile,
  botProfile,
  message 
};
