import React from "react"
import { Provider } from "react-redux"
import { store, history } from "./redux/store"
import PublicRoutes from "./router"
import { ThemeProvider } from "styled-components"
import { LocaleProvider } from "antd"
import { ApolloProvider } from "react-apollo"
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks"
import { IntlProvider } from "react-intl"
import themes from "./settings/themes"
import AppLocale from "./languageProvider"
import config, { getCurrentLanguage } from "./containers/LanguageSwitcher/config"
import { themeConfig } from "./settings"
import DashAppHolder from "./dashAppStyle"
import client from "./apolloClient"
import Boot from "./redux/boot"

const currentAppLocale = AppLocale[getCurrentLanguage(config.defaultLanguage || "english").locale]

const DashApp = () => (
  <ApolloProvider client={client}>
    <ApolloHooksProvider client={client}>
      <LocaleProvider locale={currentAppLocale.antd}>
        <IntlProvider locale={currentAppLocale.locale} messages={currentAppLocale.messages}>
          <ThemeProvider theme={themes[themeConfig.theme]}>
            <DashAppHolder>
              <Provider store={store}>
                <PublicRoutes history={history} />
              </Provider>
            </DashAppHolder>
          </ThemeProvider>
        </IntlProvider>
      </LocaleProvider>
    </ApolloHooksProvider>
  </ApolloProvider>
)

Boot()
  .then(() => DashApp())
  .catch((error) => console.error(error))

export default DashApp
export { AppLocale }
