import { all, takeEvery, put, call } from "redux-saga/effects";
import actions from "./actions";
import { delay } from "redux-saga";
import ChatHelper from "../../services/chat";
import { adminInfo, message } from "../../settings";

function* initChat() {
  const chatRooms = yield call(ChatHelper.userList);
  const conversations =
    chatRooms.length === 0
      ? []
      : yield call(ChatHelper.conversationsList, chatRooms[0].id);
  yield put({
    type: actions.CHAT_INIT_SAGA,
    chatRooms,
    conversations
  });
}

function* initMessage({ payload }) {
  const param = {
    body: payload.newConversationText,
    from_id: adminInfo.id,
    to_id: payload.chatRoom.id
  };
  const result = yield call(ChatHelper.initMessage, param);
  if (result && !result.error) {
    yield delay(800);
    yield put({
      type: actions.CHAT_UPDATE_CHATROOM_SAGA,
      payload
    });
  }
}

function* replyToMessage({ payload }) {
  const messages = yield call(ChatHelper.replyToMessage, payload);
  yield put({
    type: actions.GET_SAGA_MESSAGE,
    messages
  });
}

function* getMessage({ payload }) {
  const { conversationId } = payload;
  const messages = yield call(ChatHelper.getMessage, conversationId);
  yield put({
    type: actions.GET_SAGA_MESSAGE,
    messages
  });
}

function* updateChatroomSaga({ payload }) {
  const { chatRoom } = payload;
  let { selected } = payload;
  const conversations = yield call(ChatHelper.conversationsList, chatRoom.id);
  yield put({
    type: actions.CHAT_UPDATE_CHATROOM,
    chatRoom,
    conversations,
    selected
  });
}

export default function* rootSaga() {
  yield all([
    takeEvery(actions.CHAT_INIT, initChat),
    takeEvery(actions.INIT_MESSAGE, initMessage),
    takeEvery(actions.REPLY_TO_MESSAGE, replyToMessage),
    takeEvery(actions.GET_MESSAGE, getMessage),
    takeEvery(actions.CHAT_UPDATE_CHATROOM_SAGA, updateChatroomSaga)
  ]);
}
