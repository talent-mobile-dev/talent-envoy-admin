import clone from "clone";
import _ from "lodash";
import actions from "./actions";
 
const initState = {
  users: null,
  chatRooms: [],
  conversations: [],
  redirect: false,
  messages: [],
  selectedChatRoom: null,
  selectedConversation: null,
  openCompose: false,
  viewProfile: false,
  composedId: null,
  loading: true,
  error: false,
  mobileActiveList: true,
  mobileActiveProfile: false,
  restoringData: false,
  addNewUsersProp: {
    modalActive: false
  },
  newMessages: []
};
const sortTimeStamp = (optionA, optionB) =>
  optionB.lastMessageTime - optionA.lastMessageTime;
export default function reducer(state = initState, action) {
  switch (action.type) {
    case actions.CHAT_INIT_SAGA: {
      return {
        ...state,
        chatRooms: action.chatRooms,
        selectedChatRoom: action.chatRooms[0],
        conversations: action.conversations,
        loading: false
      };
    }
    case actions.SET_REDIRECT: {
      return {
        ...state,
        redirect: action.payload
      };
    }
    case actions.GET_MESSAGE: {
      return {
        ...state,
        selectedConversation: action.payload.conversationId
      };
    }
    case actions.GET_SAGA_MESSAGE: {
      return {
        ...state,
        messages: action.messages
      };
    }
    case actions.CLEAR_MESSAGES: {
      return {
        ...state,
        messages: []
      };
    }
    case actions.PUT_NEW_MESSAGE: {
      return {
        ...state,
        newMessages: state.newMessages.concat(action.message)
      };
    }
    case actions.ADD_NEW_MESSAGE: {
      return {
        ...state,
        messages: state.messages.concat(action.message)
      };
    }
    case actions.REMOVE_NEW_MESSAGE: {
      if (action.payload === "all") {
        return {
          ...state,
          newMessages: []
        };
      } else {
        const arr = state.newMessages.concat();
        _.remove(arr, item => {
          return item.id === action.payload;
        });

        return {
          ...state,
          newMessages: arr
        };
      }
    }
    case actions.REMOVE_NEW_MESSAGE_CONV: {
      const arr = state.newMessages.concat();
      _.remove(arr, item => {
        return item.conv_id === action.payload;
      });

      return {
        ...state,
        newMessages: arr
      };
    }
    case actions.CHAT_UPDATE_CHATROOM: {
      const chatRooms = clone(state.chatRooms);
      const { chatRoom, conversations, selected } = action;
      const { id, lastMessage, lastMessageTime } = chatRoom;
      chatRooms.forEach((chatroom, index) => {
        if (chatroom.id === id) {
          chatRooms[index].lastMessage = lastMessage;
          chatRooms[index].lastMessageTime = lastMessageTime;
        }
      });
      return {
        ...state,
        chatRooms: chatRooms.sort(sortTimeStamp),
        selectedChatRoom: selected ? chatRoom : state.selectedChatRoom,
        conversations
      };
    }
    case actions.CHAT_TOGGLE_COMPOSE:
      return {
        ...state,
        openCompose: !state.openCompose,
        viewProfile: false
      };
    case actions.CHAT_SET_TOGGLE_COMPOSED_ID:
      return {
        ...state,
        composedId: action.id
      };
    case actions.CHAT_SET_TOGGLE_VIEW_PROFILE:
      return {
        ...state,
        viewProfile: action.viewProfile
      };
    case actions.TOGGLE_MOBILE_LIST:
      return {
        ...state,
        mobileActiveList: action.mobileActiveList
      };
    case actions.TOGGLE_MOBILE_PROFILE:
      return {
        ...state,
        mobileActiveProfile: action.mobileActiveProfile
      };

    case actions.UPDATE_NEW_USER_PROPS:
      return {
        ...state,
        addNewUsersProp: action.addNewUsersProp
      };
    case actions.NEW_MESSAGE_SUCCESFULL:
      return {
        ...state,
        openCompose: false,
        composedId: null
      };

    default:
      return state;
  }
}
