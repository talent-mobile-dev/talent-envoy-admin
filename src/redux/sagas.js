import { all } from 'redux-saga/effects';
import authSagas from './auth/saga';
import mailSagas from './mail/saga';
import chatSagas from "./chat/sagas";


export default function* rootSaga(getState) {
  yield all([
    authSagas(),
    mailSagas(),
    chatSagas(),
  ]);
}
