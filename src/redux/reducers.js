import Auth from './auth/reducer';
import App from './app/reducer';
import Mails from './mail/reducer';
import Chat from "./chat/reducers";

export default {
  Auth,
  App,
  Mails,
  Chat
};
