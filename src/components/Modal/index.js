import React, { Component } from "react";
import { Modal } from "antd";
import { ModalStyle } from "./style";

export default class ModalPops extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { visible, onOk, OnCancel, children, className, title } = this.props;
    return (
      <ModalStyle>
        <Modal
          title={title}
          centered
          visible={visible}
          onOk={onOk}
          onCancel={OnCancel}
          footer={null}
          className={`modal ${className}`}
        >
          {children}
        </Modal>
      </ModalStyle>
    );
  }
}
