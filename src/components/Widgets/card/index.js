import React, { Component } from "react";
import CardWidgetWrapper from "./style";
import { Icon } from "antd";

export default class extends Component {
  render() {
    const { bgcolor, number, text, iconType } = this.props;
    const cardStyle = {
      background: bgcolor
    };
    const iconStyle = {
      position: "absolute",
      right: "25px",
      bottom: "30%",
      color: "white",
      fontSize: "18px",
      cursor: "pointer"
    };

    return (
      <CardWidgetWrapper className="isoCardWidget" style={cardStyle}>
        <div className="isoContentWrapper">
          <span className="isoLabel">{text}</span>
          <h3 className="isoStatNumber">{number}</h3>
          <Icon type={iconType} style={iconStyle} />
        </div>
      </CardWidgetWrapper>
    );
  }
}
