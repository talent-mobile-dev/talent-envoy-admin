import styled from "styled-components";
import { palette } from "styled-theme";
import WithDirection from "../../../settings/withDirection";

const CardWidgetWrapper = styled.div`
  width: 100%;
  min-height: 126px;
  padding: 20px 30px 20px 20px;
  overflow: hidden;
  display: flex;
  align-items: center;
  border-radius: 5px;
  margin-bottom: 34px;
  position: relative;

  .isoContentWrapper {
    .isoStatNumber {
      color: ${palette("invision", 3)};
      margin: 0;
      padding-top: 4px;
      font-size: 32px;
      font-weight: bold;
      line-height: 41px;
    }

    .isoLabel {
      color: ${palette("invision", 3)};
      margin: 0;
      // font-family: "Circular Std";
      font-size: 14px;
      font-weight: 400;
      line-height: 10px;
      text-transform: capitalize;
    }
  }
`;

export default WithDirection(CardWidgetWrapper);
