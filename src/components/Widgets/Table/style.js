import styled from "styled-components";
import { palette } from "styled-theme";
import WithDirection from "../../../settings/withDirection";

const NewSignUpWidget = styled.div`
  background: ${palette("invision", 3)};
  border: 1px solid #eeee;
  border-radius: 5px;
  ._tableTitle {
    color: #252525;
    font-size: 20px;
    font-weight: 500 !important;
    line-height: 25px;
    padding: 30px;
    padding-bottom: 10px;
    ._viewAll {
      color: #23b1d3;
      float: right;
      cursor: pointer;
    }
  }
  .ant-table-thead > tr > th .ant-table-column-sorter {
    left: 10px;
    // top: 21px;
  }
  .ant-table-thead > tr > th {
    background: white !important;
    color: #aaaaaa;
    font-size: 12px;
    font-weight: 500;
    line-height: 15px;
    border-radius: 4px;
    padding-left: 30px;
    text-transform: uppercase;
  }

  .ant-table-tbody > tr:nth-child(even) {
    background-color: #f9f9f9;
  }
  .ant-table-content .ant-table-body {
    overflow-x: auto;
  }
  .ant-table-tbody > tr > td {
    min-width: 65px;
    padding-left: 10px;
    padding-right: 10px;
    color: #454545;
    font-size: 14px;
    font-weight: 400;
    line-height: 18px;

    box-shadow: 0 1px 0 0 #eeeeee;
    border: none;
  }
`;

export default WithDirection(NewSignUpWidget);
