import React, { Component } from "react";
import { Table } from "antd";
import NewSignUpWidget from "./style";
import { withRouter } from "react-router";
import { TableHeader } from "../../../components/Header";

class TableVariation extends Component {
  constructor(props) {
    super(props);
    this.handleViewLink = this.handleViewLink.bind(this);
  }

  handleViewLink() {
    this.props.history.push("/dashboard/candidates");
  }
  render() {
    const { columns, dataSource, onChange, loading, isHeader } = this.props;
    return (
      <React.Fragment>
        <NewSignUpWidget className="new-signup-widget" style={{ border: 0 }}>
          {isHeader && (
            <TableHeader
              title="All New Sign Up"
              handleViewLink={this.handleViewLink}
            />
          )}
          <Table
            bordered={false}
            columns={columns}
            dataSource={dataSource}
            onChange={onChange}
            pagination={true}
            loading={loading}
            // title={() => 'All New Sign Up'}
          />
        </NewSignUpWidget>
      </React.Fragment>
    );
  }
}

export default withRouter(TableVariation);
