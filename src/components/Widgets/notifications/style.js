import styled from "styled-components";
import { palette } from "styled-theme";
import WithDirection from "../../../settings/withDirection";

const NofificationsWidget = styled.div`
  background: ${palette("invision", 3)};
  border-radius: 5px;
  border: 1px solid #eeeeee;
  margin-top: 20px;
  margin-bottom: 20px;
  ul {
    padding-left: 0px;
    li {
      padding-left: 30px;
      padding-top: 15px;
      padding-bottom: 5px;
      box-shadow: 0 1px 0 0 #eeeeee;
      list-style-type: none;
      position: relative;
      h4 {
        color: #454545;
        font-size: 16px;
        font-weight: 500;
        line-height: 20px;
      }
      p {
        color: #454545;
        font-size: 14px;
        font-weight: 400;
        line-height: 18px;
        .iconLeft {
          color: red;
          padding-right: 5px;
          position: relative;
          top: 0px;
        }
      }
      .iconRight {
        position: absolute;
        right: 22px;
        top: 34px;
        cursor: pointer;
        font-size: 14px;
      }
    }
  }
`;

export default WithDirection(NofificationsWidget);
