import React, { Component } from "react";
import NotificationsWidget from "./style";
import { withRouter } from "react-router";
import { Icon } from "antd";
import { TableHeader } from "../../../components/Header";

class Notifications extends Component {
  constructor(props) {
    super(props);
    this.handleViewLink = this.handleViewLink.bind(this);
  }

  handleViewLink() {
    alert("Notification under construction :)");
  }

  renderNotification({ name, icon, description }) {
    return (
      <div>
        <h4>{name}</h4>
        <p>
          <Icon type={icon} className="iconLeft" />
          {description}
        </p>
        <Icon type="right" className="iconRight" />
      </div>
    );
  }
  render() {
    return (
      <NotificationsWidget>
        <TableHeader
          title="Notifications"
          handleViewLink={this.handleViewLink}
        />
        <ul>
          {[0, 1, 2, 3, 4, 5, 6].map(item => (
            <li key={item}>
              {this.renderNotification({
                name: "John Smith",
                description: "Did not answer the phone 5 times",
                icon: "phone"
              })}
            </li>
          ))}
        </ul>
      </NotificationsWidget>
    );
  }
}

export default withRouter(Notifications);
