import React, { Component } from "react";
import { ViewProfileWrapper, SingleInfoWrapper } from "./viewProfile.style";
import avatar from "../../image/avatar.jpeg";
import { Button, Icon } from "antd";

const SingleInfo = ({ title, value }) => (
  <SingleInfoWrapper>
    <span className="viewProfileTitle">{title}</span>
    <span className="viewProfileValue">{value}</span>
  </SingleInfoWrapper>
);
export default class extends Component {
  render() {
    const {
      viewProfile,
      toggleViewProfile,
      toggleMobileProfile
      // chatInitiate
    } = this.props;
    if (!viewProfile) {
      return <div />;
    }
    const { otherUserInfo, location, companies, email } = viewProfile;
    const company =
      companies.companies.length === 0 ? "None" : companies.companies[0].name;
    const loc = location.city_name + ", " + location.country_name;
    return (
      <ViewProfileWrapper>
        <div className="viewProfileTopBar" style={{ height: "35px" }}>
          Contact Info
          <span
            onClick={() => {
              if (toggleMobileProfile) {
                toggleMobileProfile(false);
              }
              toggleViewProfile(false);
            }}
          >
            <Icon type="close" />
          </span>
        </div>
        <div className="viewProfileContent">
          <div className="viewProfileImage">
            <img
              alt="#"
              src={
                otherUserInfo.profileImageUrl
                  ? otherUserInfo.profileImageUrl
                  : avatar
              }
            />
            <h1>{otherUserInfo.name}</h1>
          </div>
          <div className="viewProfileQuickInfo">
            <SingleInfo title="Name" value={otherUserInfo.name} />
            <SingleInfo title="Email Address" value={email} />
            <SingleInfo title="Company" value={company} />
            <SingleInfo title="Location" value={loc} />
          </div>
          {/* <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <Button type="primary" onClick={() => chatInitiate()}>
              Initiate Conversation
            </Button>
          </div> */}
        </div>
      </ViewProfileWrapper>
    );
  }
}
