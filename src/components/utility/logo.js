import React from "react";
import { Link } from "react-router-dom";
import { siteConfig } from "../../settings";

export default ({ collapsed }) => {
  return (
    <div className="isoLogoWrapper">
      {collapsed ? (
        <div>
          <h3>
            <Link to="/dashboard">
              <img src={siteConfig.siteIcon} alt="logoIcon" width="45px" />
              {/* <i className={siteConfig.siteIcon} /> */}
            </Link>
          </h3>
        </div>
      ) : (
        <h3>
          <Link to="/dashboard">
            <img
              src={siteConfig.siteLogo}
              alt="logo"
              width="180px"
              style={{ marginRight: 25 }}
            />
          </Link>
        </h3>
      )}
    </div>
  );
};
