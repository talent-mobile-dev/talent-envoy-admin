import styled from "styled-components";
import { palette } from "styled-theme";

const LayoutContentStyle = styled.div`
  width: 100%;
  height: 100%;
`;

export default LayoutContentStyle;
