import styled, { css } from "styled-components";
import { palette } from "styled-theme/dist";

export const ContentHeaderStyle = styled.div`
  padding: 0px 30px;
  background: #fff;
  height: 90px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border: 1px solid #eeeeee;

  ${({ isTrue }) =>
    isTrue &&
    css`
      margin: 0px -15px;
    `};

  ._Headertitle {
    h2 {
      color: #252525;
      margin: 0;
      font-weight: 400;
    }
  }
  ._HeaderButtonGroup {
    margin: 0;
    li {
      margin-right: 20px;
      display: inline;
      list-style-type: none;
      .btn-addNewCandidate {
        background: ${palette("invision", 4)};
        border-color: ${palette("invision", 4)};
        font-size: 16px;
        font-weight: 400;
        letter-spacing: normal;
      }
      .btn-sendNotification {
        background: ${palette("invision", 5)};
        border-color: ${palette("invision", 5)};
        color: ${palette("invision", 6)};
        font-size: 16px;
        font-weight: 400;
        letter-spacing: normal;
      }
      i {
        font-size: 25px;
        line-height: 0;
        position: relative;
        top: 5px;
        cursor: pointer;
      }
    }
    li:last-child {
      margin: 0 !important;
    }
  }

  .header-chat-button {
      height: 40px;
      width: 177px;
      border-radius: 5px;
      background-color: #0139BA;
    color:white;
    border:none;
  }
`;

export const TableHeaderStyle = styled.div`
  ._title {
    color: #252525;
    font-size: 20px;
    font-weight: 400 !important;
    line-height: 25px;
    padding: 30px;
    padding-bottom: 10px;
    ._viewAll {
      color: #23b1d3;
      float: right;
      cursor: pointer;
    }
  }
  ul {
    padding-left: 0px;
    li {
      padding-left: 30px;
      padding-top: 15px;
      padding-bottom: 5px;
      box-shadow: 0 1px 0 0 #eeeeee;
      list-style-type: none;
      position: relative;
      h4 {
        color: #454545;
        font-size: 16px;
        font-weight: 400;
        line-height: 20px;
      }
      p {
        color: #454545;
        font-size: 14px;
        font-weight: 400;
        line-height: 18px;
        .iconLeft {
          color: red;
          padding-right: 5px;
          position: relative;
          top: 0px;
        }
      }
      .iconRight {
        position: absolute;
        right: 22px;
        top: 34px;
        cursor: pointer;
        font-size: 14px;
      }
    }
  }
`;
