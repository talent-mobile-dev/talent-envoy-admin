import React from "react";
import { Button, Icon } from "antd";
import { ContentHeaderStyle, TableHeaderStyle } from "./style";

export const ContentHeader = ({ title, renderPopup }) => {
  return (
    <ContentHeaderStyle isTrue>
      <div className="_Headertitle">
        <h2> {title}</h2>
      </div>
      <div>
        <ul className="_HeaderButtonGroup">
          <li>
            <Icon type="search" />
          </li>
          <li>
            <Button
              type="primary"
              size="large"
              className="btn-sendNotification"
              onClick={() => renderPopup("send__notification")}
            >
              Send Notification
            </Button>
          </li>
          <li>
            <Button
              type="primary"
              size="large"
              className="btn-addNewCandidate"
              onClick={() => renderPopup("add_new_candidate")}
            >
              Add New Candidate
            </Button>
          </li>
        </ul>
      </div>
    </ContentHeaderStyle>
  );
};

export const TableHeader = ({ title, handleViewLink }) => {
  return (
    <TableHeaderStyle>
      <h2 className="_title">
        {title}
        <span className="_viewAll" onClick={handleViewLink}>
          View All
        </span>
      </h2>
    </TableHeaderStyle>
  );
};
