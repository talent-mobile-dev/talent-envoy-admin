import React from "react";
import { Button, Icon } from "antd";
import { ContentHeaderStyle, TableHeaderStyle } from "./style";

export const ContentHeader = ({ title, renderPopup }) => {
  return (
    <ContentHeaderStyle isTrue>
      <div className="_Headertitle">
        <h2> {title}</h2>
      </div>
      <div>
        <ul className="_HeaderButtonGroup">
          <li>
            {/* <button onClick={()=>{alert("aaa")}} className="header-chat-button">New Conversation</button> */}
          </li>
        </ul>
      </div>
    </ContentHeaderStyle>
  );
};

export const TableHeader = ({ title, handleViewLink }) => {
  return (
    <TableHeaderStyle>
      <h2 className="_title">
        {title}
        <span className="_viewAll" onClick={handleViewLink}>
          View All
        </span>
      </h2>
    </TableHeaderStyle>
  );
};
